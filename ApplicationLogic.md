# App Flow
=========

In this document server refers to the signaling server.

## JSON Message Schema for Messages sent from Client to Server or Server to Client:

```javascript
	{ 
		'content': the actual data to be transmitted, 
		'type': message type; will be used by receiving Client or Server to determine how to process the message 'content'. The types are defined in the LAN_COMMUNICATOR_MSG_TYPES object, 
		'time_sent': the time the message was sent at the sender (Client or Server) 
	}
```
	
The signaling server contains two major objects that help facilitate the conferencing functionality:
	- var meeting_rooms = {}; 	  // Contains all the meeting room objects. Each key is a server-generated
								  // unique integer meeting room id
	- var connected_clients = {}; // Contains all the active websocket connection objects each representing a  
								  // websocket connection between the server and each client that has connected
								  // to it. Each key is a server-generated unique integer sig_serv_conn_id value
	Sample Structure:
	```javascript
		connected_clients = {
			'<sig_serv_conn_id_1>' : WebSocketConnectionObject,
			..........,
			'<sig_serv_conn_id_n>' : WebSocketConnectionObject
		};
	```
	
Client Connection to Signaling Server:
	- each client that connects to the webserver
		* Receives the app's home-page.
			* The received home-page contains js code that connects to the signaling server via a websocket connection
				* upon successful connection the following happens in the WebSocketServer.onconnection handler (ie. webSockSvrConnectionHandler(socket_connection)). This handler gets called when a new WebSocket connection is established:
					- the server stores the ip address of the client in a new field (`my_ip_addy`) which it adds to the Websocket connection object.
					
					- the server generates a unique integer value and stores it in a new field (`sig_serv_conn_id`) which it adds to the Websocket connection object. This will serve as the server assigned websocket identifier.
					
					- the server then stores the Websocket connection object in the connected_clients object using the `sig_serv_conn_id` value generated in the previous step as the key (i.e something like connected_clients[socket_connection.sig_serv_conn_id] = socket_connection).
                                        
                    - the server then sends json data containing the newly generated `sig_serv_conn_id` value in a message with a type value of LAN_COMMUNICATOR_MSG_TYPES.TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR to the client
                        - the client then stores the received `sig_serv_conn_id` value in a `sig_serv_conn_id` property in its WebSocket connection object

					- the server then sends json data containing a list of existing meeting rooms in a message with a type value of LAN_COMMUNICATOR_MSG_TYPES.INITIALIZE_M_ROOMS to the client
						- for the first client to connect to the server the json data will indicate that no meeting room exists
						- if rooms exists the json data will contain a list of the rooms and the participants associated with each room
						- the client updates their UI with room list data from the server
                        - the client also adds video elements to the video page for all other potential clients
                        - the client then asks for the user's webcam permission and plugs the video stream into a section of the video page

Room States:

- 1 participant only (ie. the creator): waiting for at least one or at most three more participant(s) to join.
	- the participant can LEAVE the room (implicitly destroying it) or just keep waiting for other participants 
	
- 2 participants in room: at least one or at most two more participant(s) can join.
	- the two participants can continue the meeting & finally each LEAVE to destroy the meeting room when they are both done.
	- if one or two participants join, they will all have to LEAVE in order to destroy the meeting room when they are all done.
	
- 3 participants in room: at least and most one more participant(s) can join.
	- the three participants can continue the meeting & finally each LEAVE to destroy the meeting room when they are all done.
	- if one participant joins, they will all have to LEAVE in order to destroy the meeting room when they are all done.
	
- 4 participants in room: room is full, no more participant can join.
	- They each have to LEAVE in order to destroy the meeting room
	- 1 to 3 of them can LEAVE which will make the room available to 1 to 3 other new participants

						
Creating a Meeting Room:
	- Below is the structure of a meeting room object:
		Room object:
			room_id: int
			room_name: string
			participants: array of participant objects
				participant object
					sig_serv_conn_id: int
					participant_name: string
					is_creator: boolean
					
	- A form with a room name field and the creating participant's name field is used to gather input on the client's end
	- Client validates inputted data and sends a create room message(ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM) to the server with the room name and creating participant's name
	- Upon receiveing the create room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM), the server will perform the following operations:
		- create a room object:
			- assign a unique integer id to the room (room_id)
			- set room_name to the room name supplied in the create room message
			- set the participants field of the room object to an empty array
			- create a participant object 
				- set participant_name to the creating participant's name supplied in the create room message
				- use the value of the Websocket connection object's sig_serv_conn_id field (accessible via this.sig_serv_conn_id in the onmessage event handler of the Websocket connection object) to set the value of the sig_serv_conn_id field in the participant object
				- set the is_creator field to true
			- add the participant object to the participants array of the Room object we just created
		
		- Add the created Room Object to the meeting_rooms object with the room_id value as the key
		- server should send an add room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.ADD_NEW_ROOM_TO_CLIENT_UI) back to all connected clients
			- This add room message will cause each client to update their UI.
			
			
Join Room Action
    - Participant names within a meeting room must be unique (there must be validation of this when a user tries to join a meeting room)
    - Client clicks the Join button
    	- the click button handler: 
    		- validates that the participant name field is not empty
    		- then validates that the value in the participant name field is not an existing participant name in the room to be joined
    		- if all validation passes
    			- the client sends a join room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM) to the server; this message contains the room_id of the room to be joined and the participant name entered by the client.
    			- upon receiving the join room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM), the server locates the room object with the specified room_id in the meeting_rooms object and then adds a new participant with the specified participant name to the participants array of the located room object
    			- the server then sends back the updated room object to all connected clients in a joined room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI)

Join Room Action - WebRTC
    - the joiner / caller initiates the WebRTC call to all other clients in the desired room 
        - the caller MUST include his/her sig_serv_conn_id value in each of the messages to the server
        - the caller is the only one that sends the webrtc offer (ie. via the createOffer(..) method) to other clients in the room
            - the code in case "offer" will never be executed on the client that is the caller
            - the code in case "offer" will be executed on every other client that is not the caller
        - when the server receives a webrtc related message from the joiner, the server must broadcast to all other clients in the room.
        


Leave Room Action
    LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM & LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI

    - Client clicks the leave button
        - the click handler sends the room_id and participant name in a leave room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM) to the server
        - upon receiving the leave room message, the server locates the room object with the specified room_id in the meeting_rooms object and then deletes the participant object in the located room object with the name matching the specified participant name.
            - if after deleting the specified participant from the room object the participants array becomes empty then
                - the meeting room object itself should be deleted from the meeting_rooms object
        - the server 
            - then sends back the updated meeting room object to all connected clients if it still has one or more participants in an update left room message (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI) 
                - client receives the updated meeting room object and updates its UI
            - else no more participants, server should send a delete room from ui (ie. with a type value of LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI) message back to all connected clients 
			
			


Client Disconnects from Signaling Server:
	- When a client disconnects and the websocket.connection.onclose event is triggered which actually gets handled by webSockConnOnCloseHandler(exit_code, exit_reason)
		* go through the meeting rooms in the meeting_rooms object and delete each participant from the participants array of each meeting room having the same `sig_serv_conn_id` value as the `sig_serv_conn_id` value of the disconncting socket
		* remove the client's Websocket Connection object from the connected_clients object


On the client side: when there is a connection error, display message on screen








