# Server Side

```javascript
var connected_clients = {
    '<sig_serv_conn_id_1>' : WebSocketConnectionObject,
    ..........,
    '<sig_serv_conn_id_n>' : WebSocketConnectionObject
};
```

## JSON Message Schema for Messages sent from Client to Server or Server to Client:

```javascript
{ 
    'content': the actual data to be transmitted, 
    'type': message type; will be used by receiving Client or Server to determine how to process the message 'content', 
    'time_sent': the time the message was sent at the sender (Client or Server) 
}
```

Room object:
    room_id: int
    room_name: string
    participants: array of participant objects
        participant object
            sig_serv_conn_id: int
            participant_name: string
            is_creator: boolean

Client Messages to Server
============================
- _doAddMeetingRoomButtonClick(): LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM
	//send data to server to create new room
	var new_room_object = {};
	new_room_object['room_name'] = ui_el_room_name_txt_box.value;
	new_room_object['creators_name'] = ui_el_creators_name_txt_box.value;
	_sendToServer(connection, new_room_object, LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM);
	
- _doLeaveRoomButtonClick(room_id, participant_name): LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM
    var leave_room_message_to_svr = {};
    leave_room_message_to_svr['room_id'] = room_id;
    leave_room_message_to_svr['participant_name'] = participant_name;
	_sendToServer(connection, leave_room_message_to_svr, LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM);
	
- _doJoinRoomButtonClick(room_id): LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM
	//send data to server to add participant to room
	var join_room_message_object = {};
	join_room_message_object['room_id'] = room_id;
	join_room_message_object['participant_name'] = ui_el_participant_name_txt_box.value;
	_sendToServer(connection, join_room_message_object, LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM);

- makeCreateOfferHandler(potential_conn_id, room_id): LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER
    return function(offer) {
        local_webrtc_data[room_id][potential_conn_id]['peer_conn_obj'].setLocalDescription(offer);
        _sendToServer(
            connection,
            {
                "offer": offer,
                room_id: room_id,
                senders_sig_serv_conn_id: parseInt(connection.sig_serv_conn_id),
                receivers_sig_serv_conn_id: parseInt(potential_conn_id)
            },
            LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER
        );
    };

- makeCreateAnswerHandler (room_id, senders_conn_id, receivers_conn_id): LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER
    return function(answer) {
        local_webrtc_data[room_id][senders_conn_id]['peer_conn_obj'].setLocalDescription(answer);
        _sendToServer(
            connection,
            {
                "answer": answer,
                room_id: room_id,
                senders_sig_serv_conn_id: receivers_conn_id,
                receivers_sig_serv_conn_id: senders_conn_id
            },
            LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER
        );             
    };
	
- makeOnIceCandidateHandler(room_id, conn_id): LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
    return function (event) {
        if (event.candidate) {
            _sendToServer(
                connection,
                {
                    candidate: event.candidate,
                    room_id: room_id,
                    senders_sig_serv_conn_id: parseInt(connection.sig_serv_conn_id),
                    receivers_sig_serv_conn_id: parseInt(conn_id)
                },
                LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
            );
        }
    };
	
Server Messages to Client
==========================
- webSockSvrConnectionHandler(socket_connection): LAN_COMMUNICATOR_MSG_TYPES.INITIALIZE_M_ROOMS
    // Send rooms object to client, so that the client can update its user-interface.
    _sendToClient(
        socket_connection, meeting_rooms, LAN_COMMUNICATOR_MSG_TYPES.INITIALIZE_M_ROOMS
    );

- webSockSvrConnectionHandler(socket_connection): LAN_COMMUNICATOR_MSG_TYPES.TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR
    _sendToClient(
        socket_connection,
        { "sig_serv_conn_id" : socket_connection.sig_serv_conn_id },
        LAN_COMMUNICATOR_MSG_TYPES.TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR
    );

- case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM: LAN_COMMUNICATOR_MSG_TYPES.ADD_NEW_ROOM_TO_CLIENT_UI
	for (var sig_serv_conn_id in connected_clients) {
		_sendToClient(
			connected_clients[sig_serv_conn_id],
			{ 'new_room_object' : new_room_object, 'creators_sig_serv_conn_id': this.sig_serv_conn_id },
			LAN_COMMUNICATOR_MSG_TYPES.ADD_NEW_ROOM_TO_CLIENT_UI
		);
	}

- webSockConnOnCloseHandler(exit_code, exit_reason): LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI
	else {
		// send back the updated meeting room object to other connected 
		// clients since there are still one or more participant(s) in 
		// meeting_rooms[leave_room_message_object.room_id].participants
		// Send the message with a type value of 
		// LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI.
		for (var sig_serv_conn_id in connected_clients) {

			if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) ) {

				_sendToClient(
					connected_clients[sig_serv_conn_id],
					{ 
						"updated_meeting_room" : meeting_rooms[room_id],
						"leavers_sig_serv_conn_id" : this.sig_serv_conn_id
					},
					LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI
				);
			} // if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) )
		} // for (var sig_serv_conn_id in connected_clients)
	} // if( meeting_rooms[room_id].participants.length <= 0 ) ... else
	// if( index_of_participant_to_remove !== -1 )
	// for (var room_id in meeting_rooms)
	
- case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI
	else {             
		// send back the updated meeting room object to all
		// connected clients since there are still one or more participant(s) 
		// in meeting_rooms[leave_room_message_object.room_id].participants
		// Send the message with a type value of LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI.
		for (var sig_serv_conn_id in connected_clients) {

			_sendToClient(
				connected_clients[sig_serv_conn_id],
				{ 
					"updated_meeting_room" : meeting_rooms[leave_room_message_object.room_id],
					"leavers_sig_serv_conn_id" : this.sig_serv_conn_id
				},
				LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI
			);
		}
	} //if( meeting_rooms[leave_room_message_object.room_id].participants.length <= 0 ) ... else
	// if( index_of_participant_to_leave !== -1 )  ... else
	//if ( meeting_rooms.hasOwnProperty(leave_room_message_object.room_id) ) ... else
	
- webSockConnOnCloseHandler(exit_code, exit_reason): LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI
    // delete the rooms marked for deletion because 
    // they no longer have participant(s) in them.
    room_ids_for_deletion.forEach(
        function( room_id ) {
            delete meeting_rooms[room_id];
            
            // send message back to other connected clients to delete room 
            // from their user-interfaces (UIs).
            for (var sig_serv_conn_id in connected_clients) {
                if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) ) {
                    _sendToClient(
                        connected_clients[sig_serv_conn_id],
                        { "room_id" : room_id },
                        LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI
                    );
                } // if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) )
            } // for (var sig_serv_conn_id in connected_clients)
        },
        this
    );
	
- case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI
	if( meeting_rooms[leave_room_message_object.room_id].participants.length <= 0 ) {
		// the deleted participant was the last participant in the meeting room. 
		// since we can't have a meeting room without any participant, we have 
		// to delete the room without any participant, we have to delete the
		// meeting room from the meeting_rooms object.
		delete meeting_rooms[leave_room_message_object.room_id];

		// send message back to all connected clients to delete room 
		// from their user-interfaces (UIs).
		for (var sig_serv_conn_id in connected_clients) {
			_sendToClient(
				connected_clients[sig_serv_conn_id],
				{ "room_id" : leave_room_message_object.room_id },
				LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI
			);
		}
	}
	
- case LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM: LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI
	var join_room_message_object = received_message_object.content;
	
	if ( meeting_rooms.hasOwnProperty(join_room_message_object.room_id) ) {
		//////////////////////////////////////////////
		// a room with the sent meeting room id exists
		//////////////////////////////////////////////
		//add a new participant
		var participant_obj = {};
		participant_obj['sig_serv_conn_id'] = this.sig_serv_conn_id;
		participant_obj['participant_name'] = join_room_message_object.participant_name;
		participant_obj['is_creator'] = false;

		//add the participant object to the participants array
		//of the room object
		meeting_rooms[join_room_message_object.room_id].participants.push(participant_obj);

		// send back the updated meeting room object to all
		// connected clients.
		// Send the message with a type value of 
		// LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI.
		for (var sig_serv_conn_id in connected_clients) {
			_sendToClient(
				connected_clients[sig_serv_conn_id],
				{ 
					"updated_meeting_room" : meeting_rooms[join_room_message_object.room_id],
					"joiners_sig_serv_conn_id" : this.sig_serv_conn_id
				},
				LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI
			);
		}
	} 

- case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER: LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER 
	var webrtc_offer_object = received_message_object.content;
	//forward offer to specified client
	_sendToClient(
		connected_clients[webrtc_offer_object.receivers_sig_serv_conn_id],
		webrtc_offer_object, LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER
	);
	
- case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER: LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER
	var webrtc_answer_object = received_message_object.content;
	//forward answer to specified client
	_sendToClient(
		connected_clients[webrtc_answer_object.receivers_sig_serv_conn_id],
		webrtc_answer_object,
		LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER
	);
	
- case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE: LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
	var webrtc_candidate_object = received_message_object.content;
	//forward candidate to specified client
	_sendToClient(
		connected_clients[webrtc_candidate_object.receivers_sig_serv_conn_id],
		webrtc_candidate_object,
		LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
	);