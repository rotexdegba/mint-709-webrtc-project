## Key Directories
* **`actual-project-code`:** Contains source code for the project.

  * **`actual-project-code/3rd-party-code-used`:** Contains source code from 3rd parties that are used in the project.
  * **`actual-project-code/my-code`:** Source code written by me for the project.

* **`referenced-code`:** Contains 3rd party source code that consulted for example on how to implement certain features in the application. Source code in this directory will never be part of the code that runs the application. The code here is meant for the instructor's purpose when grading the project.

## Key Files
* **`README.md`:** Contains a high level description of the project and the various components and how they are to be assembled together.
* **`TODO.md`:** Contains a list of features or tasks to perform in order to complete the project.

## Installation Instructions
- It is strongly recommended that you install this app in a debian flavored linux server like Ubuntu
- Install the latest nodejs LTS version
- Change directory to _**`actual-project-code`**_
- Install _**`nginx`**_ which will be used as the webserver:

    > You should edit `./nginx-mint-709-webrtc-project.conf` to suit your needs

    > _`sudo apt-get install nginx-full`_ # install the webserver

    > _`sudo cp ./nginx-mint-709-webrtc-project.conf /etc/nginx/sites-enabled/nginx-mint-709-webrtc-project`_

    > _`sudo rm /etc/nginx/sites-enabled/default`_ # remove the default configuration

    > _`sudo service nginx reload`_ # make the server load the new configuration

    - At this point you should be able to browse to `https://X.X.X.X/` to view the home-page of this application. Note that X.X.X.X should replaced with the IP address of your server. You will also need to add a security exception for the site to your chrome and firefox browsers (you will be warned by the browser the first time you try to access the url.) because we are using a self-sign SSL certificate for the https connection. For firefox, you should additionally access `https://X.X.X.X:8888` to get the prompt to add a security excpetion for the underlying secure websocket connection (wss://).


- Install the _**`ws`**_ (https://www.npmjs.com/package/ws) package which will be used to build the Signaling Server

    > _`sudo npm install -g ws`_

- to run the signaling server, execute the command below (you current working directory should be _**`actual-project-code`**_) :

    > _`node ./signaling-server.js`_

## How to Generate Self-signed SSL Certificates (Linux)

- Run the commands below:

  > _`openssl genrsa -out key.pem 2048`_

  > _`openssl req -new -sha256 -key key.pem -out csr.pem`_

  > _`openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem`_

  > _`rm csr.pem`_

  - This should leave you with two files, cert.pem (the certificate) and key.pem (the private key). That's all you need for an SSL connection.
