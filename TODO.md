# Features
---
### Signaling / Coordinating server
* `**[DONE]**`~~ Introduce the concept of rooms to keep track of on-going session(s) between groups of 2 to 4 clients / users.~~
* (This might be redundant with the current architecture) keep track of all users available to communicate (ie. either not in a room or in a room with less than 4 users) and users that are not available for communication (ie. they are in an active session in a full room).
* (This might be redundant with the current architecture) The creator of a room (ie. the first user to initiate a session between one to three other users) should be able to kick a person out of a room.
* Also allow a room to be able to have a password, which must be communicated by the room creator to the other users intended to join a room
* Server should try to detect client's bandwidth capability and whether or not the client is a mobile or desktop / laptop device.  
* `**[DONE]**`~~ Implement a logging function that writes to a file, so that the log file can be examined for debugging purposes.~~

### Client-side (Browser Webpages)
* Report device type to signaling server if possible

---
# Other Tasks
---
* ~~Since the last quarter of 2015, Google Chrome now mandates that all WebRTC applications must be run using HTTPS. As a result I have to configure SSL for my webserver.~~ `**[DONE]**`
 * ~~I should also configure WSS for the websocket connection between the clients (web-browsers) and the signaling server.~~ `**[DONE]**`

* ~~Make sure to document installation and setup steps for the webserver and signalling server.~~ `**[WORK IN PROGRESS IN README.MD]**`

* A post mint-709-project completion feature will be screen-sharing

* Look at example here (http://www.sitepoint.com/sending-emails-gmail-javascript-api/) in case the system may need emailing functionality

* CLEANUP: delete /actual-project-code/public/c05 , I was using it to study how the sample client code for WebRTC works

* Implement LAN_COMMUNICATOR_MSG_TYPES.LOG_MSG_FROM_CLIENT_TO_FILE , this will allow the client to send log messages to the server 
 * use JSON.stringify(msg) at the client side before sending the message for logging


* onaddstream is deprecated! Use peerConnection.ontrack instead. See RTCPeerConnection.addTrack() also.
 * chrome does not currently support peerConnection.ontrack. Wait until chrome supports it.
 * code in _uiSetupWebRtcPeerConnections(updated_meeting_room_object, joiners_conn_id) needs to be updated fix this

* Look into replacing the `connected_clients` object with the built-in wss.clients property to improve memory usage
 * main thing to keep in mind is that `connected_clients` is an object indexed on sig_serv_id, while wss.clients is an array
 * should do a memory profile to see if switching to wss.clients will use less memory

* Tag each video box with participant's name

* Update sample room with full slots html in client and index.html

* Use a library like https://www.webrtc-experiment.com/DetectRTC/ to detect if browser supports webrtc and display error message if necessary

* Update log statements in the server script so that large objects are not getting repeatedly or unecessarily logged, which will lead to huge log files

* use the .focus() method to focus UI elements where necessary