/**
 *  Student ID: 011273515
 *  Name: Adedurotimi Adegbamigbe
 *  Course: MINT 709 Capstone Project
 *  
 *  This is the client portion of a WebRTC web-application that allows 
 *  video-conferencing sessions between 2 to maximum of 4 clients.
 */
var was_conn_successfully_established = false;

//hide app pages initially until connection to the websocket server is succesful
var conferencing_app_pages = document.querySelector('#conferencing-app-pages');
_uiHideElement(conferencing_app_pages);

//var connection = new WebSocket('wss://192.139.7.43:8888');  // vm on work pc
//var connection = new WebSocket('wss://192.168.1.105:8888'); // vm on msi laptop on upstairs router
//var connection = new WebSocket('wss://192.168.0.105:8888'); // vm on msi laptop on portable router
var connection = new WebSocket('wss://192.168.0.104:8888'); // vm on thinkpad laptop on portable router

var LAN_COMMUNICATOR_MSG_TYPES = {
                         "INITIALIZE_M_ROOMS" : 'init-m-rooms',
    "TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR" : 'tag-client-websockconn-with-id-from-svr',
                              "CREATE_M_ROOM" : 'create-m-room',
                  "ADD_NEW_ROOM_TO_CLIENT_UI" : 'add-new-room-to-client-ui',
                                 "LEAVE_ROOM" : 'leave-room',
              "UPDATE_LEFT_ROOM_ON_CLIENT_UI" : 'update-left-room-on-client-ui',
                        "DELETE_ROOM_FROM_UI" : 'delete-room-from-ui',
                                  "JOIN_ROOM" : 'join-room',
            "UPDATE_JOINED_ROOM_ON_CLIENT_UI" : 'update-joined-room-on-client-ui',
                               "WEBRTC_OFFER" : 'webrtc-offer',
                              "WEBRTC_ANSWER" : 'webrtc-answer',
                           "WEBRTC_CANDIDATE" : 'webrtc-candidate',
                                    "GENERIC" : 'generic'
};

var MAX_NUM_PARTICIPANTS_IN_ROOM = 4;

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
// START: CODE FOR MANIPULATING THE USER INTERFACE
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

// User Interface Element Objects

// Existing Meeting Rooms Page / Home Page
var ui_el_meeting_rooms_page = document.querySelector('#meeting-rooms-page'); // div element
var ui_el_no_meeting_rooms_h2 = document.querySelector('#no-meeting-rooms');
var ui_el_existing_meeting_rooms_div = document.querySelector('#existing-meeting-rooms');
var ui_el_existing_meeting_rooms_container_div = document.querySelector('#existing-meeting-rooms-container');
var ui_el_create_new_meeting_room_button = document.querySelector('#btn-create-new-meeting-room');

// handle create new meeting room button click
ui_el_create_new_meeting_room_button.onclick = _doCreateNewMeetingRoomButtonClick;

// Create New Meeting Room Page
var ui_el_create_meeting_room_page = document.querySelector('#create-meeting-room-form'); // div element
var ui_el_room_name_txt_box = document.querySelector('#new-meeting-room-name'); // textbox element
var ui_el_creators_name_txt_box = document.querySelector('#creating-participants-name'); // textbox element
var ui_el_add_meeting_room_button = document.querySelector('#btn-add-meeting-room');

// handle add meeting room button click
ui_el_add_meeting_room_button.onclick = _doAddMeetingRoomButtonClick;

// Video Page
var ui_el_video_page = document.querySelector('#video-page'); // div element
var ui_el_video_page_container = document.querySelector('#video-container'); // div element
var ui_el_video_page_room_name_h2 = document.querySelector('#video-page-room-name'); // h2 element
var ui_el_my_video_el = document.getElementById('video0'); // video element for client's video stream
var ui_el_video_page_leave_room_button = document.querySelector('#hangup-button');
_uiHideAndDisableElement(ui_el_video_page_leave_room_button); // hide the leave meeting button in the 
                                                              // video section when the page is loaded.
// No Camera and Mic Error Page
var ui_el_no_cam_mic_page = document.querySelector('#no-cam-mic-page'); // div element
_uiHideElement(ui_el_no_cam_mic_page);
                                                              // video section when the page is loaded.
// Server Connection Failed Error Page
var ui_el_server_conn_failed_page = document.querySelector('#server-connection-failed-page'); // div element
_uiHideElement(ui_el_server_conn_failed_page);

var my_local_stream = null; // global reference to video stream obtained from clients camera
                            // this same stream is fed into the client's video element on-screen

var local_webrtc_data = {}; // object to hold all data relevant to an ongoing video session.
                            // contents will include instances of RTCPeerConnection, etc.
                            // Each entry is indexed by a room_id value. Eg.
                            // 
                            //    local_webrtc_data[room_id][partcs_conn_id]['peer_conn_obj'] = new RTCPeerConnection(servers);
                            //    local_webrtc_data[room_id][partcs_conn_id]['participant_obj'] = participant;              
                            //    local_webrtc_data[room_id][partcs_conn_id]['has_been_sent_offer'] = false;                
                            //    local_webrtc_data[room_id][partcs_conn_id]['has_recieved_offer'] = false;                
                            //    local_webrtc_data[room_id][partcs_conn_id]['has_recieved_answer'] = false;
                            // 
                            // Where room_id is a room id value and partcs_conn_id is
                            // connection id value for a particpant (a.k.a) client 
                            // other than the current client that the webrtc data 
                            // is being created for.
var offerOptions = {
  offerToReceiveAudio: 0,
  offerToReceiveVideo: 1
};

/**
 * Checks if an element is enabled on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {Boolean} true if el is enabled, else false.
 */
function _uiElementIsEnabled(el) {
    
    return el.style.disabled === false;
}

/**
 * Checks if an element is visible on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {Boolean} true if el is visible, else false.
 */
function _uiElementIsVisible(el) {
    
    return el.style.display !== 'none' && el.style.visibility !== 'hidden';
}

/**
 * Hides and disables an element on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {void}
 */
function _uiHideAndDisableElement(el) {
    _uiHideElement(el);
    _uiDisableElement(el);
}

/**
 * Makes an element visible and also enables the element on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {void}
 */
function _uiShowAndEnableElement(el) {
    _uiShowElement(el);
    _uiEnableElement(el);
}

/**
 * Hides an element on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {void}
 */
function _uiHideElement(el) { 
    el.style.display = 'none';
    el.style.visibility = 'hidden';
}

/**
 * Makes an element visible on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {void}
 */
function _uiShowElement(el) { 
    el.style.display = 'block'; 
    el.style.visibility = 'visible';
}

/**
 * Disables an element on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {void}
 */
function _uiDisableElement(el) { el.style.disabled = true; }

/**
 * Enables an element on the screen.
 * 
 * @param {Element} el see https://developer.mozilla.org/en-US/docs/Web/API/Element
 * 
 * @returns {void}
 */
function _uiEnableElement(el) { el.style.disabled = false; }

/**
 * Retrieves camera stream and plugs it into a video element on the screen.
 * 
 * @returns {void}
 */
function _streamMyCameraToMyScreen() {
    
    _log2Console('function _streamMyCameraToMyScreen()');
    
    if( my_local_stream === null ) {
        
        _log2Console('function _streamMyCameraToMyScreen(): Requesting local stream');
        
        navigator.mediaDevices.getUserMedia({
            audio: false,
            video: true
        })
        .then(_gotStream)
        .catch(function (e) {
            _uiShowElement(ui_el_no_cam_mic_page);
            _uiHideElement(ui_el_video_page);
            _uiHideElement(ui_el_meeting_rooms_page);
            _uiHideElement(ui_el_create_meeting_room_page);
            alert('getUserMedia() error: ' + e.name);
            _log2Console('getUserMedia() error: ' + e.name);
        });
    }
}

/**
 * Plugs a stream into the client's video element on the screen.
 * 
 * @param {MediaStream} stream see https://developer.mozilla.org/en-US/docs/Web/API/MediaStream
 * 
 * @returns {void}
 */
function _gotStream(stream) {
    
    _log2Console('function gotStream(stream): Received local stream');
    ui_el_my_video_el.srcObject = stream;
    _log2Console('function gotStream(stream): After ui_el_my_video_el.srcObject = stream;');
    
    // Add stream to global scope so it's accessible from the browser console
    window.my_local_stream = my_local_stream = stream;
}

/**
 * Send webrtc peer connection offers to other participants in the specified room.
 * 
 * @param {String|int} room_id identifier for room containing participants that
 *                             offers will be sent to.
 * 
 * @returns {void}
 */
function _sendWebRtcOffersToOtherParticipants(room_id) {
    
    _log2Console('_sendWebRtcOffersToOtherParticipants(`' + room_id + '`)');
    
    // make sure there is webrtc data for the specified room before proceeding
    if ( local_webrtc_data.hasOwnProperty(room_id) ) {
        
        // iterate over local_webrtc_data and send offers to other peers
        for ( var potential_conn_id in local_webrtc_data[room_id] ) {

            if (
                local_webrtc_data[room_id].hasOwnProperty(potential_conn_id)
                && !(local_webrtc_data[room_id][potential_conn_id]['has_been_sent_offer'])
            ) {
                // potential_conn_id is the current participant's sig_serv_conn_id value
                local_webrtc_data[room_id][potential_conn_id]['has_been_sent_offer'] = true;

                _log2Console(
                    '_sendWebRtcOffersToOtherParticipants(`' + room_id + '`): SENDING OFFER TO CONN ' + potential_conn_id
                );

                // Send the offer to the participant through the signaling server.
                local_webrtc_data[room_id][potential_conn_id]['peer_conn_obj']
                    .createOffer(offerOptions)
                    .then(
                        _makeCreateOfferHandler(potential_conn_id, room_id),
                        _makeCreateOfferErrorHandler(potential_conn_id)
                    );
            } // if ( local_webrtc_data[room_id].hasOwnProperty(potential_conn_id) && !(local_webrtc_data[room_id][potential_conn_id]['has_been_sent_offer']) )
        } // for ( var potential_conn_id in local_webrtc_data[room_id] )
    } // if ( local_webrtc_data.hasOwnProperty(room_id) )
}

/**
 * Returns a function that handles what happens after 
 * RTCPeerConnection.createOffer(offerOptions) has been successfully 
 * called without any error(s) occuring. The returned function sends 
 * the created offer to the intended participant via the signaling server.
 * 
 * @param {int} potential_conn_id used to get at the webrtc data for the 
 *                                participant whom an offer is being sent to.
 * @param {String|int} room_id identifier used to get at the webrtc data 
 *                             for the room containing the participant 
 *                             whom an offer is being sent to.
 *                             
 * @returns {Function}
 */
function _makeCreateOfferHandler(potential_conn_id, room_id) {
    
    _log2Console('_makeCreateOfferHandler(potential_conn_id, room_id):', potential_conn_id, ', ', room_id);
    
    return function(offer) {
        
        _log2Console('createOffer Handler for room: ', room_id, ' and conn: ', potential_conn_id, " about to >>>>setLocalDescription<<<< with created offer: \n", offer);
        
        // set the local description on the participant's peer connection object
        local_webrtc_data[room_id][potential_conn_id]['peer_conn_obj'].setLocalDescription(offer);

        _log2Console('createOffer Handler for room: ', room_id, ' and conn: ', potential_conn_id, " >>>>SENDING<<<< created offer: \n", offer);
        
        // send offer to the participant via the signaling server
        _sendToServer(
            connection,
            {
                "offer": offer,
                room_id: room_id,
                senders_sig_serv_conn_id: parseInt(connection.sig_serv_conn_id),
                receivers_sig_serv_conn_id: parseInt(potential_conn_id)
            },
            LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER
        );
        
        _log2Console('createOffer Handler for room: ', room_id, ' and conn: ', potential_conn_id, " >>>>SENT<<<< created offer: \n", offer);
    };
}

/**
 * Returns a function that handles what happens after 
 * RTCPeerConnection.createOffer(offerOptions) has been called and one or more 
 * error(s) were generated. The returned function displays an error message 
 * (it also logs the message to the console).
 * 
 * @param {int} potential_conn_id can be used to get at the webrtc data for the 
 *                                participant whom an offer was sent to.
 *                                
 * @returns {Function}
 */
function _makeCreateOfferErrorHandler(potential_conn_id) {
    
    _log2Console("_makeCreateOfferErrorHandler("+ potential_conn_id + ")");
    
    return function(reason) {
        alert('in Create Offer Error Handler: Error Occurred When Creating Offer to Transmit Video to Conn `' + potential_conn_id + '`.');
        _log2Console('in Create Offer Error Handler: Error Occurred When Creating Offer to Transmit Video to Conn `' + potential_conn_id + '` :', reason);
    };
}

/**
 * Cleans up webrtc data objects associated with a specified room and then 
 * deletes all webrtc data for the specified room. Also removes video 
 * elements for other participants in the room from the screen.
 * 
 * @param {String|int} room_id the identifier for the room whose webrtc data is 
 *                             to be deleted.
 *                             
 * @returns {void}
 */
function _endWebRtcCallUponDeleteRoom(room_id) {
    
    _log2Console('_endWebRtcCallUponDeleteRoom(`', room_id, '`): For All Conns in room ', room_id);

    // make sure there is webrtc data for the specified room before proceeding
    if ( local_webrtc_data.hasOwnProperty(room_id) ) {
        
        // iterate over local_webrtc_data for the other participants in the room
        for (var potential_conn_id in local_webrtc_data[room_id]) {
            
            if ( local_webrtc_data[room_id].hasOwnProperty(potential_conn_id) ) {
                // potential_conn_id is the current participant's sig_serv_conn_id value
                
                // close the peer connection for the client that just left.
                local_webrtc_data[room_id][potential_conn_id]['peer_conn_obj'].close();
                
                // get the video box (element) for the participant in the 
                // current iteration in the loop.
                var leavers_video_elem = document.getElementById('video' + potential_conn_id);
                
                if (leavers_video_elem !== null) {
                    
                    // remove the participant's video box from the screen
                    ui_el_video_page_container.removeChild(leavers_video_elem);
                    
                } // if (leavers_video_elem !== null)
                
                // delete the webrtc data for the participant
                delete local_webrtc_data[room_id][potential_conn_id];
                
            } // if ( local_webrtc_data[room_id].hasOwnProperty(potential_conn_id) )
        } // for (var potential_conn_id in local_webrtc_data[room_id])
        
        // delete all webrtc data for the room
        delete local_webrtc_data[room_id];
        
        //clear the room name from the video section
        ui_el_video_page_room_name_h2.innerHTML = '';
        
    } // if ( local_webrtc_data.hasOwnProperty(room_id) ) 
    
    _log2Console('_endWebRtcCallUponDeleteRoom(`', room_id, '`): local_webrtc_data after deletion of room ', room_id, ' data: ', local_webrtc_data);
}

/**
 * Cleans up webrtc data objects associated with a specified participant in a
 * room and then deletes all webrtc data for the specified participant in the 
 * room. Also removes the video element for the participant from the screen.
 * The specified participant is a participant that is leaving the room.
 * 
 * @param {String|int} room_id the identifier for the room from which a leaving 
 *                             participant's webrtc data is to be deleted.
 * @param {int} leavers_conn_id the identifier for the getting at the data for 
 *                              the participant whose webrtc data is to be 
 *                              deleted from the specified room.
 *                              
 * @returns {void}
 */
function _endWebRtcCallForLeavingParticipant(room_id, leavers_conn_id) {
    
    _log2Console('_endWebRtcCallForLeavingParticipant(', room_id, ", ", leavers_conn_id, '): For Conn ', leavers_conn_id, ' in room ', room_id);
    
    if (
        // make sure there is webrtc data for the specified participant in the 
        // specified room.
        local_webrtc_data.hasOwnProperty(room_id)
        && local_webrtc_data[room_id].hasOwnProperty(leavers_conn_id)
    ) {
        // close the peer connection for the client that just left.
        local_webrtc_data[room_id][leavers_conn_id]['peer_conn_obj'].close();
        
        // get the leaving participant's video box from the screen
        var leavers_video_elem = document.getElementById('video' + leavers_conn_id);
        
        if( leavers_video_elem !== null ) {
            // remove the leaving participant's video box from the screen
            ui_el_video_page_container.removeChild(leavers_video_elem);
        }
        
        // delete the webrtc data for the leaving participant
        delete local_webrtc_data[room_id][leavers_conn_id];
        
        //clear the room name from the video section
        ui_el_video_page_room_name_h2.innerHTML = '';
    }
    
    _log2Console('_endWebRtcCallForLeavingParticipant(', room_id, ", ", leavers_conn_id, '): local_webrtc_data[', room_id, '] after deletion: ', local_webrtc_data[room_id]);
    _log2Console('_endWebRtcCallForLeavingParticipant(', room_id, ", ", leavers_conn_id, '): local_webrtc_data after deletion: ', local_webrtc_data);
}

/**
 * Displays the existing meeting rooms (on the signaling server), 
 * each with corresponding participants.
 * 
 * @param {Object} exisiting_m_rooms_obj an object containing exisitng meeting rooms' data.
 * 
 * @returns {void}
 */
function _uiIntializeMeetingRooms(exisiting_m_rooms_obj) {
    
    _log2Console("_uiIntializeMeetingRooms(exisiting_m_rooms_obj): \n", exisiting_m_rooms_obj);
    
    if( _objectHasAnyProperty(exisiting_m_rooms_obj) ) {
        
        // at least one meeting room exists on the server
        
        // hide server connection failed error page
        _uiHideElement(ui_el_server_conn_failed_page);
        
        // hide no camera / mic error page
        _uiHideElement(ui_el_no_cam_mic_page);
        
        // hide Create New Meeting Room page
        _uiHideElement(ui_el_create_meeting_room_page);
        
        // hide "No Meeting Rooms Exist" header
        _uiHideElement(ui_el_no_meeting_rooms_h2);
        
        var rooms_html = '';
/*
        <!-- sample room with open slots and leave button (meaning the current client is a participant in the room) -->
        <div id="room-4" class="bordered room">
            <p class="room-name-header">
                <strong>Room Name:</strong> Room <span class="bold italic blue-text"> (1 Participant, 3 Slots Open)</span> 
                 <button type="button" onclick="_doLeaveRoomButtonClick(4, &quot;Creator&quot;);">Leave</button> 
            </p>
            <hr>
            <ul>
                <li>Creator</li> 
            </ul>
        </div>
        
        <!-- sample room with open slots and join button (meaning the current client is not a participant in the room) -->
        <div id="room-5" class="bordered room">
            <p class="room-name-header">
                <strong>Room Name:</strong> Room <span class="bold italic blue-text"> (1 Participant, 3 Slots Open)</span> 
                <br><br><strong>Participant Name:</strong> <input id="participant-name-for-room-5" type="text" value="">
                 <span class="red-text" id="participant-name-for-room-5-error"></span> 
                 <button type="button" onclick="_doJoinRoomButtonClick(5);">Join</button> 
            </p>
            <hr>
            <ul>
                <li>Some Participant</li> 
            </ul>
        </div>

        <!-- sample room with full slots and leave button (meaning the current client is a participant in the room) -->
        <!-- if the current client isn't a participant in the room there would be no leave button nor a join button -->
        <!-- since this is a full room -->
        <div id="room-1" class="bordered room">	
            <p class="room-name-header">
                <strong>Room Name:</strong> Hallo <span class="bold italic blue-text"> (4 Participants, 0 Slots Open)</span> 
                <span class="bold red-text">FULL ROOM</span> 
                <button type="button" onclick="_doLeaveRoomButtonClick(1, &quot;P 1&quot;);">Leave</button> 
            </p>
            <hr>
            <ul>
                <li>P 1</li> 
                <li>P 2</li> 
                <li>P3</li> 
                <li>P 4</li> 
            </ul>
        </div>
*/
        // loop through existing meeting room objects
        for ( var key in exisiting_m_rooms_obj ) {
            
            // skip loop if the property is from prototype
            if ( !exisiting_m_rooms_obj.hasOwnProperty(key) ) { continue; }

            var meeting_room_obj = exisiting_m_rooms_obj[key]; // current meeting
                                                               // room object.
            var num_participants_in_room = meeting_room_obj.participants.length;
            var slots_open_in_room = MAX_NUM_PARTICIPANTS_IN_ROOM - num_participants_in_room;
            
            var num_participants_in_room_str = 
                    ( num_participants_in_room > 1 )
                        ? ' (' + num_participants_in_room + ' Participants, '
                        : ' (' + num_participants_in_room + ' Participant, ';
            
            var slots_open_in_room_str = 
                    ( slots_open_in_room === 1 )
                        ? slots_open_in_room + ' Slot Open)'
                        : slots_open_in_room + ' Slots Open)';
            
            rooms_html += '<div id="room-' + meeting_room_obj.room_id + '" class="bordered room">' + "\n";
            rooms_html += "\t" + '<p class="room-name-header">' + "\n";
            rooms_html += "\t\t" + '<strong>Room Name:' + '</strong> ' + meeting_room_obj.room_name + ' <span class="bold italic blue-text">' + num_participants_in_room_str + slots_open_in_room_str + "</span> \n";
            
            if( slots_open_in_room >= 1 ) {
                
                // generate texbox for typing in the participant name for a
                // new participant.
                rooms_html += "\t\t" + '<br><br><strong>Participant Name:</strong> <input id="participant-name-for-room-' + meeting_room_obj.room_id + '" type="text" value="" />' + "\n";
                
                // generate placeholder element for displaying validation
                // errors related to the Participant Name field.
                rooms_html += "\t\t" + ' <span class="red-text" id="participant-name-for-room-' + meeting_room_obj.room_id + '-error" ></span> ' + "\n";
                
                // generate 'Join' button that will be clicked in order to join
                // the current meeting room.
                rooms_html += "\t\t" + ' <button type="button" onclick="_doJoinRoomButtonClick(' + meeting_room_obj.room_id + ');">Join</button> ' + "\n";
                
            } else {
                
                // room is full
                rooms_html += "\t\t" + '<span class="bold red-text">FULL ROOM</span> ' + "\n";
            }
            
            rooms_html += "\t" + '</p>' + "\n";
            rooms_html += "\t" + '<hr>' + "\n";
            
            rooms_html += "\t" + '<ul>' + "\n";
            
            // loop through participants and dump them in the ul
            meeting_room_obj.participants.forEach(function (participant) {
                rooms_html += "\t\t" + '<li>' + participant.participant_name + '</li> ' + "\n";
            });
            
            rooms_html += "\t" + '</ul>' + "\n";
            
            rooms_html += '</div>' + "\n";
        }
        
        // add all the generated html to the Existing Meeting Rooms section
        // of the screen.
        ui_el_existing_meeting_rooms_container_div.innerHTML = rooms_html;
        
    } else {
        
        // no meeting room exists yet
        
        // Hide Existing Meeting Rooms section of the screen
        _uiHideElement(ui_el_existing_meeting_rooms_div);
        
        // hide Create New Meeting Room page
        _uiHideElement(ui_el_create_meeting_room_page);
    }
}

/**
 * Handle the "Create New Meeting Room" button's click event.
 * 
 * @returns {void}
 */
function _doCreateNewMeetingRoomButtonClick() {
    
    _log2Console("_doCreateNewMeetingRoomButtonClick()");
    _uiHideElement(ui_el_meeting_rooms_page);
    _uiShowElement(ui_el_create_meeting_room_page);
}

/**
 * Handle the "Add Meeting Room" button's click event.
 * 
 * @returns {void}
 */
function _doAddMeetingRoomButtonClick() {

    _log2Console("_doAddMeetingRoomButtonClick(): uiEl_RoomNameTxtBox.value = ", ui_el_room_name_txt_box.value);
    _log2Console("_doAddMeetingRoomButtonClick(): uiEl_CreatorsNameTxtBox.value = ", ui_el_creators_name_txt_box.value);
    
    ///////////////////////////////////////
    // validate form values
    ///////////////////////////////////////
    var all_validation_passed = true;
    
    if( ui_el_room_name_txt_box.value.length <= 0 ) {
        
        // Empty Room Name
        document.getElementById("new-meeting-room-name-error").innerHTML = "Can't be Empty!";
        all_validation_passed = false;
        
    } else {
        
        // Valid. Clear previous error message
        document.getElementById("new-meeting-room-name-error").innerHTML = '';
    }
    
    if( ui_el_creators_name_txt_box.value.length <= 0 ) {
        
        // Empty Creator's Name
        document.getElementById("creating-participants-name-error").innerHTML = "Can't be Empty!";
        all_validation_passed = false;
        
    } else {
        
        // Valid. Clear previous error message
        document.getElementById("creating-participants-name-error").innerHTML = '';
    }
        
    if( all_validation_passed ) {
        
        // send data to server to create new room
        var new_room_object = {};
        new_room_object['room_name'] = ui_el_room_name_txt_box.value;
        new_room_object['creators_name'] = ui_el_creators_name_txt_box.value;
        _sendToServer(connection, new_room_object, LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM);
    }
}

/**
 * Handle the "Join" button's click event. 
 * 
 * Validates and sends entered participant name  and specified room_id to the
 * signaling server so that the server can add the participant to the room.
 * 
 * @param {String|int} room_id identifier for the room to be joined.
 * 
 * @returns {void}
 */
function _doJoinRoomButtonClick(room_id) {
    
    var current_rooms_container = document.getElementById('room-' + room_id);
    var ui_el_participant_name_txt_box = document.getElementById('participant-name-for-room-' + room_id);
    var ui_el_participant_name_txt_box_error_field = document.getElementById('participant-name-for-room-' + room_id + '-error');
    _log2Console("_doJoinRoomButtonClick(", room_id, "): ui_el_participant_name_txt_box.value = ", ui_el_participant_name_txt_box.value);
    
    ///////////////////////////////////////
    // validate form values
    ///////////////////////////////////////
    var all_validation_passed = true;
    
    if( ui_el_participant_name_txt_box.value.length <= 0 ) {
        
        // Empty Participant Name
        ui_el_participant_name_txt_box_error_field.innerHTML = "Can't be Empty!";
        all_validation_passed = false;
        
    } else {
        
        // Valid. Clear previous error message
        ui_el_participant_name_txt_box_error_field.innerHTML = '';
        
        // get list items containing existing participant names in the 
        // specified room
        var existing_participant_names_lis = 
                current_rooms_container.getElementsByTagName('li');
        
        // loop through list items containing existing participant names 
        // in the specified room
        for ( var i=0; (i < existing_participant_names_lis.length) && all_validation_passed; i++ ) {
            
           // make sure specified name does not already exist 
           if(
                ui_el_participant_name_txt_box.value.trim() 
                === existing_participant_names_lis[i].textContent.trim()
            ) {
                ui_el_participant_name_txt_box_error_field.innerHTML = "Name already Exists!";
                all_validation_passed = false;
            }
        }
    }
        
    if( all_validation_passed ) {
        
        // send data to server to add participant to room
        var join_room_message_object = {};
        join_room_message_object['room_id'] = room_id;
        join_room_message_object['participant_name'] = ui_el_participant_name_txt_box.value;
        _sendToServer(connection, join_room_message_object, LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM);
    }
}

/**
 * Handle the "Leave" button's click event.
 * This button is displayed in the "Existing Meeting Rooms" section beside each
 * meeting room the user is a participant in.
 *  
 * @param {String|int} room_id identifier for the room to be departed from.
 * @param {String} participant_name name of the participant departing from room.
 * 
 * @returns {void}
 */
function _doLeaveRoomButtonClick(room_id, participant_name) {
    
    _log2Console("_doLeaveRoomButtonClick('"+ room_id + "', '" + participant_name + "')");
    
    var leave_room_message_to_svr = {};
    leave_room_message_to_svr['room_id'] = room_id;
    leave_room_message_to_svr['participant_name'] = participant_name;
    
    // get all video elements on screen
    var all_video_elems = document.querySelectorAll("#video-container video");
    var len = all_video_elems.length;

    // remove every video box in the video section except mine
    for (var i = 0; i < len; i++) {

        // make sure the video element does not belong to me
        if( all_video_elems[i].id !== ui_el_my_video_el.id ) {

            ui_el_video_page_container.removeChild(all_video_elems[i]);
        }
    }
    
    // send leave room message to the server
    _sendToServer(connection, leave_room_message_to_svr, LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM);
}

/**
 * Returns a function that handles "Leave Meeting" button's click event.
 * This button is in the video section at the top.
 * 
 * @param {String|int} room_id identifier for the room to be departed from.
 * @param {String} participant_name name of the participant departing from room.
 * 
 * @returns {Function}
 */
function _makeLeaveMeetingButtonClickHandler4VideoPage(room_id, participant_name) {
    
    _log2Console("_makeLeaveMeetingButtonClickHandler4VideoPage('"+ room_id + "', '" + participant_name + "')");
    
    return function() {
        
        _log2Console("Leave Meeting Button Clicked!");
        
        if( _uiElementIsVisible(ui_el_video_page_leave_room_button) ) {
            
            // hide and disable the "Leave Meeting" button in the video section
            _uiHideAndDisableElement(ui_el_video_page_leave_room_button);
        }
        
        // reveal the section containing the list of existing meeting rooms
        _uiShowElement(ui_el_meeting_rooms_page);
        
        // execute code to leave the room
        _doLeaveRoomButtonClick(room_id, participant_name);
    };
}

/**
 * Add newly created meeting room to the screen.
 * 
 * @param {Object} new_m_room_obj object representing newly created meeting room.
 * @param {int} creators_conn_id identifier for the creator of the new meeting room.
 * 
 * @returns {void}
 */
function _uiAddNewMeetingRoom(new_m_room_obj, creators_conn_id) {
    
    _log2Console("_uiAddNewMeetingRoom(new_m_room_obj, creators_conn_id): new_m_room_obj: \n\n", new_m_room_obj, "\n\n creators_conn_id: ", creators_conn_id );
    
    if( _objectHasAnyProperty(new_m_room_obj) ) {
        
        var new_room_html = '';
        var num_participants_in_room = new_m_room_obj.participants.length;
        var slots_open_in_room = MAX_NUM_PARTICIPANTS_IN_ROOM - num_participants_in_room;

        var num_participants_in_room_str = 
                ( num_participants_in_room > 1 )
                    ? ' (' + num_participants_in_room + ' Participants, '
                    : ' (' + num_participants_in_room + ' Participant, ';

        var slots_open_in_room_str = 
                ( slots_open_in_room === 1 )
                    ? slots_open_in_room + ' Slot Open)'
                    : slots_open_in_room + ' Slots Open)';

        new_room_html += '<div id="room-' + new_m_room_obj.room_id + '" class="bordered room">' + "\n";
        new_room_html += "\t" + '<p class="room-name-header">' + "\n";
        new_room_html += "\t\t" + '<strong>Room Name:' + '</strong> ' + new_m_room_obj.room_name + ' <span class="bold italic blue-text">' + num_participants_in_room_str + slots_open_in_room_str + "</span> \n";

        if( slots_open_in_room < 1 ) {

            new_room_html += "\t\t" + '<span class="bold red-text">FULL ROOM</span> ' + "\n";
        }
        
        if( parseInt(connection.sig_serv_conn_id) === parseInt(creators_conn_id) ) {
            
            // client viewing this page is also the creator of the new room
            
            // get the name of the creator
            var participants_name = _getParticipantNameViaSigServConnId(new_m_room_obj, parseInt(connection.sig_serv_conn_id));
            
            if( participants_name !== -1 ) {
                
                // generate leave button in the "Existing Meeting Rooms" section
                new_room_html += "\t\t" + ' <button type="button" onclick="_doLeaveRoomButtonClick(' + new_m_room_obj.room_id + ", &quot;" + participants_name + "&quot;" + ');">Leave</button> ' + "\n";
                
                // add click handler to the "Leave Room" button in the video section
                ui_el_video_page_leave_room_button.onclick = 
                    _makeLeaveMeetingButtonClickHandler4VideoPage(
                        new_m_room_obj.room_id, 
                        participants_name
                    );
            
                // reveal "Leave Room" button in the video section
                _uiShowAndEnableElement(ui_el_video_page_leave_room_button);
            }
            
        } else if (
            parseInt(connection.sig_serv_conn_id) !== parseInt(creators_conn_id)
            && slots_open_in_room >= 1
        ) {
            // client viewing this page is not a participant in this meeting 
            // room and there are still slot(s) available for people to join 
            // this meeting room.
    
            // generate texbox for typing in the participant name 
            // for a new participant.
            new_room_html += "\t\t" + '<br><br><strong>Participant Name:</strong> <input id="participant-name-for-room-' + new_m_room_obj.room_id + '" type="text" value="" />' + "\n";
            
            // generate placeholder element for displaying validation errors 
            // related to the Participant Name field.
            new_room_html += "\t\t" + ' <span class="red-text" id="participant-name-for-room-' + new_m_room_obj.room_id + '-error" ></span> ' + "\n";
            
            // generate 'Join' button that will be clicked in order to join
            // the current meeting room.
            new_room_html += "\t\t" + ' <button type="button" onclick="_doJoinRoomButtonClick(' + new_m_room_obj.room_id + ');">Join</button> ' + "\n";
        }
        
        new_room_html += "\t" + '</p>' + "\n";
        new_room_html += "\t" + '<hr>' + "\n";

        new_room_html += "\t" + '<ul>' + "\n";
        
        // loop through participants and dump them in the ul
        new_m_room_obj.participants.forEach(function (participant) {
            new_room_html += "\t\t" + '<li>' + participant.participant_name + '</li> ' + "\n";
        });

        new_room_html += "\t" + '</ul>' + "\n";
        new_room_html += '</div>' + "\n";
        
        // update the screen elements
        ui_el_existing_meeting_rooms_container_div.insertAdjacentHTML('beforeend', new_room_html);
        ui_el_room_name_txt_box.value = '';
        ui_el_creators_name_txt_box.value = '';
        _uiShowElement(ui_el_meeting_rooms_page);
        _uiShowElement(ui_el_existing_meeting_rooms_div);
        _uiHideElement(ui_el_no_meeting_rooms_h2);
        _uiHideElement(ui_el_create_meeting_room_page);
        
        if( parseInt(connection.sig_serv_conn_id) === parseInt(creators_conn_id) ) {
            
            // set the room name in the video section for the current client
            // since the current client is the creator of the room
            ui_el_video_page_room_name_h2.innerHTML = 
                new_m_room_obj.room_name 
                + ': <span class="normal-text italic">Waiting for other participants ...</span>';
            
            // hide the section containing the list of existing meeting rooms
            _uiHideElement(ui_el_meeting_rooms_page);
        }
        
    } else {
        // TODO: Display Error Message that room might not have been successfully created
    }
}

/**
 * Create RTCPeerConnection objects for each of the participants in the updated
 * meeting room (excluding the participant / client currently viewing this page) 
 * if and only if the participant / client currently viewing this page is a
 * participant in the updated meeting room object.
 * 
 * @param {Object} updated_meeting_room_object a meeting room object
 * @param {int} joiners_conn_id identifier for participant that just joined the 
 *                              updated meeting room
 * 
 * @returns {void}
 */
function _uiSetupWebRtcPeerConnections(updated_meeting_room_object, joiners_conn_id) {
    
    _log2Console("_uiSetupWebRtcPeerConnections(updated_meeting_room_object, joiners_conn_id): updated_meeting_room_object: \n\n", updated_meeting_room_object, "\n\n joiners_conn_id: ", joiners_conn_id );
    
    if( _objectHasAnyProperty(updated_meeting_room_object) ) {

        var room_id = updated_meeting_room_object.room_id;

        // make sure there is more than one participant in the room
        // and the current client is a member of the room
        if( 
            updated_meeting_room_object.participants.length > 1 
            && _participantWithSigServConnIdExists(updated_meeting_room_object, connection.sig_serv_conn_id)
        ) {
            var servers = null;
            
            if( !local_webrtc_data.hasOwnProperty(room_id) ) {
                
                // initialize spot for webrtc data for the room in question
                local_webrtc_data[room_id] = {};
            }
            
            updated_meeting_room_object.participants.forEach(function (participant) {
                
                // only looking for other participants and not myself and
                // participants that don't already have a peer connection
                // object
                if( 
                    parseInt(connection.sig_serv_conn_id) !== parseInt(participant.sig_serv_conn_id)
                    && !local_webrtc_data[room_id].hasOwnProperty(participant.sig_serv_conn_id)
                ) {
                    var partcs_conn_id = participant.sig_serv_conn_id;
                    
                    // create a spot for webrtc data for the current participant
                    local_webrtc_data[room_id][partcs_conn_id] = {};
                    
                    // for debugging purposes
                    var participant_info = 'Participant: ' + JSON.stringify(participant);

                    // create one local peer connection object for
                    // the current participant and store other useful
                    // data.
                    local_webrtc_data[room_id][partcs_conn_id]['peer_conn_obj'] = new RTCPeerConnection(servers);
                    _log2Console("created local peer connection object for ", participant_info, "in local_webrtc_data['", room_id, "'][", partcs_conn_id, "]['peer_conn_obj']");
                    
                    local_webrtc_data[room_id][partcs_conn_id]['participant_obj'] = participant;              
                    local_webrtc_data[room_id][partcs_conn_id]['has_been_sent_offer'] = false;                
                    local_webrtc_data[room_id][partcs_conn_id]['has_recieved_offer'] = false;                
                    local_webrtc_data[room_id][partcs_conn_id]['has_recieved_answer'] = false;                

                    // add video element to the screen
                    var video_el_html = '<video id="video' + partcs_conn_id + '" autoplay></video>' + "\n";
                    ui_el_video_page_container.insertAdjacentHTML('beforeend', video_el_html);
                    
                    // get the video element just created for the current
                    // participant.
                    video_elem = document.getElementById('video' + partcs_conn_id);
                    
                    // add handler for the RTCPeerConnection object's 
                    // `onaddstream` event.
                    local_webrtc_data[room_id][partcs_conn_id]['peer_conn_obj']
                                                        .onaddstream = 
                                                            _makeOnAddStreamHandler(video_elem);                   
                
                    // add handler for the RTCPeerConnection object's 
                    // `onicecandidate` event.
                    local_webrtc_data[room_id][partcs_conn_id]['peer_conn_obj']
                                                        .onicecandidate = 
                                                            _makeOnIceCandidateHandler(
                                                                room_id,
                                                                partcs_conn_id
                                                            ); 
                    
                    // add current client's camera stream
                    local_webrtc_data[room_id][partcs_conn_id]['peer_conn_obj']
                                                        .addStream(my_local_stream);
 
                    _log2Console("Added local stream to local_webrtc_data['", room_id, "'][", partcs_conn_id, "]['peer_conn_obj'] for ", participant_info);
                    
                } // if( parseInt(connection.sig_serv_conn_id) !== parseInt(participant.sig_serv_conn_id) .....
            });// updated_meeting_room_object.participants.forEach(function (participant) { 
        } // if( updated_meeting_room_object.participants.length > 1 )
        
        _log2Console("_uiSetupWebRtcPeerConnections: Dumping local_webrtc_data[", room_id, "] at the end:\n", local_webrtc_data[room_id]);
    } // if( _objectHasAnyProperty(updated_meeting_room_object) )
    
    _log2Console("_uiSetupWebRtcPeerConnections: Dumping local_webrtc_data at the end:\n", local_webrtc_data);
}

/**
 * Returns a handler function for the onicecandidate event of an
 * RTCPeerConnection object
 * 
 * @param {String|int} room_id identifier for the room in question
 * @param {int} conn_id identifier for the participant in the room whose 
 *                      RTCPeerConnection object's onicecandidate candidate
 *                      property is to be assigned to the handler function 
 *                      returned by this function.
 * @returns {Function}
 */
function _makeOnIceCandidateHandler(room_id, conn_id) {
    
    _log2Console("_makeOnIceCandidateHandler('"+ room_id + "', " + conn_id + ")");
    
    return function (event) {
        
        _log2Console("in onicecandidate Handler(event): event: \n" + event);
        
        if (event.candidate) {
            
            _log2Console("in onicecandidate Handler(event): about to send candidate: \n", event.candidate);
            
            _sendToServer(
                connection,
                {
                    candidate: event.candidate,
                    room_id: room_id,
                    senders_sig_serv_conn_id: parseInt(connection.sig_serv_conn_id),
                    receivers_sig_serv_conn_id: parseInt(conn_id)
                },
                LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
            );
    
            _log2Console("in onicecandidate Handler(event): sent candidate: \n", event.candidate);
            
        } else {
            
            _log2Console("in onicecandidate Handler(event): no candidate at the moment.");
        }
    };
}

/**
 * Returns a handler function for the onaddstream 
 * event of an RTCPeerConnection object
 * 
 * @param {Element} video_elem a video element that will recieve a video stream
 * 
 * @returns {Function}
 */
function _makeOnAddStreamHandler(video_elem) {
    
    _log2Console("_makeOnAddStreamHandler(video_elem):\n\n", video_elem);
    
    return function(e) {
        video_elem.srcObject = e.stream;
        _log2Console("Video element with id `", video_elem.id, "` received remote stream!");
    };
}

/**
 * Add newly created meeting room to the screen.
 * 
 * @param {Object} updated_meeting_room_object object representing updated meeting room.
 * 
 * @returns {void}
 */
function _uiUpdateMeetingRoom(updated_meeting_room_object) {
    
    _log2Console("_uiUpdateMeetingRoom(updated_meeting_room_object): updated_meeting_room_object: \n\n", updated_meeting_room_object);
    
    if( _objectHasAnyProperty(updated_meeting_room_object) ) {
        
        var updated_room_html = '';
        var num_participants_in_room = updated_meeting_room_object.participants.length;
        var slots_open_in_room = MAX_NUM_PARTICIPANTS_IN_ROOM - num_participants_in_room;

        var num_participants_in_room_str = 
                ( num_participants_in_room > 1 )
                    ? ' (' + num_participants_in_room + ' Participants, '
                    : ' (' + num_participants_in_room + ' Participant, ';

        var slots_open_in_room_str = 
                ( slots_open_in_room === 1 )
                    ? slots_open_in_room + ' Slot Open)'
                    : slots_open_in_room + ' Slots Open)';

        updated_room_html += "\t" + '<p class="room-name-header">' + "\n";
        updated_room_html += "\t\t" + '<strong>Room Name:' + '</strong> ' + updated_meeting_room_object.room_name + ' <span class="bold italic blue-text">' + num_participants_in_room_str + slots_open_in_room_str + "</span> \n";

        if( slots_open_in_room < 1 ) {

            updated_room_html += "\t\t" + '<span class="bold red-text">FULL ROOM</span> ' + "\n";
        }
        
        if( _participantWithSigServConnIdExists(updated_meeting_room_object, connection.sig_serv_conn_id) ) {
            
            // client viewing this page is also a member of the updated room
            
            // get the participant's name
            var participants_name = _getParticipantNameViaSigServConnId(updated_meeting_room_object, parseInt(connection.sig_serv_conn_id));
            
            if( participants_name !== -1 ) {
                
                // generate leave button in the "Existing Meeting Rooms" section
                updated_room_html += "\t\t" + ' <button type="button" onclick="_doLeaveRoomButtonClick(' + updated_meeting_room_object.room_id + ", &quot;" + participants_name + "&quot;" + ');">Leave</button> ' + "\n";
                
                // add click handler to the "Leave Room" button in the video section
                ui_el_video_page_leave_room_button.onclick = 
                    _makeLeaveMeetingButtonClickHandler4VideoPage(
                        updated_meeting_room_object.room_id, 
                        participants_name
                    );
            
                // reveal "Leave Room" button in the video section
                _uiShowAndEnableElement(ui_el_video_page_leave_room_button);
            }
            
        } else if(
            slots_open_in_room >= 1
            && !_participantWithSigServConnIdExists(updated_meeting_room_object, connection.sig_serv_conn_id)
        ) {
            // client viewing this page is not a participant in this meeting 
            // room and there are still slot(s) available for people to join 
            // this meeting room.
            
            // generate texbox for typing in the participant name 
            // for a new participant.
            updated_room_html += "\t\t" + '<br><br><strong>Participant Name:</strong> <input id="participant-name-for-room-' + updated_meeting_room_object.room_id + '" type="text" value="" />' + "\n";
            
            // generate placeholder element for displaying validation errors 
            // related to the Participant Name field.
            updated_room_html += "\t\t" + ' <span class="red-text" id="participant-name-for-room-' + updated_meeting_room_object.room_id + '-error" ></span> ' + "\n";
            
            // generate 'Join' button that will be clicked in order to join
            // the current meeting room.
            updated_room_html += "\t\t" + ' <button type="button" onclick="_doJoinRoomButtonClick(' + updated_meeting_room_object.room_id + ');">Join</button> ' + "\n";
        }
        
        updated_room_html += "\t" + '</p>' + "\n";
        updated_room_html += "\t" + '<hr>' + "\n";

        updated_room_html += "\t" + '<ul>' + "\n";

        // loop through participants and dump them in the ul
        updated_meeting_room_object.participants.forEach(function (participant) {
            updated_room_html += "\t\t" + '<li>' + participant.participant_name + '</li> ' + "\n";
        });

        updated_room_html += "\t" + '</ul>' + "\n";
        
        // update the screen elements
        var room_div_id = 'room-' + updated_meeting_room_object.room_id;
        var room_div = document.getElementById(room_div_id);
        room_div.innerHTML = updated_room_html;
    }
}

/**
 * Removes specified room from screen.
 * 
 * @param {String|int} room_id identifier for room to be removed from screen
 * 
 * @returns {void}
 */
function _uiDeleteRoom(room_id) {
    
    _log2Console("_uiDeleteRoom('"+ room_id + "')");
    
    var room_div_id = 'room-' + room_id;
    var room_div = document.getElementById(room_div_id);
    
    if( room_div !== null ) {
        
        room_div.remove();
    }
    
    if( ui_el_existing_meeting_rooms_container_div.getElementsByTagName('div').length <= 0 ) {
        
        // all rooms have been deleted
        // no meeting room exists
        
        // show the "No Meeting Rooms Exist" header
        _uiShowElement(ui_el_no_meeting_rooms_h2);
        
        // hide the "Create New Meeting Room" page
        _uiHideElement(ui_el_create_meeting_room_page);
        
        // hide the "Existing Meeting Rooms" section
        _uiHideElement(ui_el_existing_meeting_rooms_div);
    }
}

/**
 * Remove the connecting to server popup and enable the display of other pages.
 * 
 * @returns {void}
 */
function _removeConnecting2ServerPopup() {
    
    // remove the connecting to server popup
    document.getElementById('connecting-2-server-overlay').remove();
    document.getElementById('connecting-2-server-modal').remove();
    _uiShowElement(conferencing_app_pages);
}


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
// END: CODE FOR MANIPULATING THE USER INTERFACE
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

connection.onopen = function (on_open_event) {
    
    was_conn_successfully_established = true;
    _removeConnecting2ServerPopup();
    
    _log2Console("connection.onopen = function (on_open_event): ", ' Number of args:', arguments.length, ' Args:', arguments);
    _sendToServer(connection, "Connected from browser!");
};

connection.onclose = function (on_close_event) {
    
    _log2Console("connection.onclose = function (on_close_event): ", ' Number of args:', arguments.length, ' Args:', arguments);
};

connection.onerror = function (on_error_event) {
    
    if( !was_conn_successfully_established ) {
        
        _removeConnecting2ServerPopup();
        _uiShowElement(ui_el_server_conn_failed_page);
        _uiHideElement(ui_el_no_cam_mic_page);
        _uiHideElement(ui_el_video_page);
        _uiHideElement(ui_el_meeting_rooms_page);
        _uiHideElement(ui_el_create_meeting_room_page);
    }
    
    _log2Console("connection.onerror = function (on_error_event): Error Triggered!", ' Number of args:', arguments.length, ' Args:', arguments);
};

connection.onmessage = function (on_msg_event) {

    _log2Console("connection.onmessage = function (on_msg_event): ", ' Number of args:', arguments.length, ' Args:', arguments);
    
    // on_msg_event is an instance of MessageEvent 
    // see https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent

    var received_message_object = _parseRecievedMsg(on_msg_event.data);
    _log2Console("Parsed Received Message: ", JSON.stringify(received_message_object));
    
    if( _isObject(received_message_object) ) {
        
        switch (received_message_object.type) {

            case LAN_COMMUNICATOR_MSG_TYPES.TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR:");
                
                var svr_generated_id_obj = received_message_object.content;
                
                // Update this WebSocket object with the server generated id
                this.sig_serv_conn_id = svr_generated_id_obj.sig_serv_conn_id;               
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.INITIALIZE_M_ROOMS:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.INITIALIZE_M_ROOMS:");
                
                var exisiting_m_rooms_obj = received_message_object.content;
                _uiIntializeMeetingRooms(exisiting_m_rooms_obj);
                _streamMyCameraToMyScreen();
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.ADD_NEW_ROOM_TO_CLIENT_UI:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.ADD_NEW_ROOM_TO_CLIENT_UI:");
                
                var new_room_object = received_message_object.content.new_room_object;
                var creators_conn_id = received_message_object.content.creators_sig_serv_conn_id;
                _uiAddNewMeetingRoom(new_room_object, creators_conn_id);
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI:");
                
                _uiDeleteRoom(received_message_object.content.room_id);
                _endWebRtcCallUponDeleteRoom(received_message_object.content.room_id);
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI:");
                
                _uiUpdateMeetingRoom(received_message_object.content.updated_meeting_room);

                if( 
                    parseInt(this.sig_serv_conn_id) === parseInt(received_message_object.content.leavers_sig_serv_conn_id)
                ) {
                    // this is the client that left the room
                    // all webrtc data should be cleared
                    _endWebRtcCallUponDeleteRoom(received_message_object.content.updated_meeting_room.room_id);
                    
                    if( _uiElementIsVisible(ui_el_video_page_leave_room_button) ) {
                        
                        // hide the leave meeting button in the
                        _uiHideAndDisableElement(ui_el_video_page_leave_room_button);
                    }
                    
                } else {
                    // only webrtc data for client that left should be cleared
                    _endWebRtcCallForLeavingParticipant(
                        received_message_object.content.updated_meeting_room.room_id,
                        received_message_object.content.leavers_sig_serv_conn_id
                    );
                }
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI:");
                
                _uiUpdateMeetingRoom(received_message_object.content.updated_meeting_room);
                _uiSetupWebRtcPeerConnections(
                    received_message_object.content.updated_meeting_room,
                    received_message_object.content.joiners_sig_serv_conn_id
                );
        
                if( 
                    _participantWithSigServConnIdExists(
                        received_message_object.content.updated_meeting_room, 
                        this.sig_serv_conn_id
                    )
                ) {
                    // the current client is a participant in the updated room
                    // update the room name in the video section
                    ui_el_video_page_room_name_h2.innerHTML = 
                        received_message_object.content.updated_meeting_room.room_name;
                
                    // hide the section containing the list of existing meeting rooms
                    _uiHideElement(ui_el_meeting_rooms_page);
                }
                
                if( 
                    parseInt(this.sig_serv_conn_id) === parseInt(received_message_object.content.joiners_sig_serv_conn_id)
                ) {
                    // this is the client that joined the room
                    // the client should initiate video transmission
                    _sendWebRtcOffersToOtherParticipants(
                        received_message_object.content.updated_meeting_room.room_id
                    );
                }
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER:");
                
                var webrtc_offer_object = received_message_object.content;
                var room_id = webrtc_offer_object.room_id;
                var senders_conn_id = webrtc_offer_object.senders_sig_serv_conn_id;
                
                if(
                    local_webrtc_data.hasOwnProperty(room_id)
                    && local_webrtc_data[room_id].hasOwnProperty(senders_conn_id)
                    && !(local_webrtc_data[room_id][senders_conn_id]['has_recieved_offer'])
                ) {
                    _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER: Recieved Offer:\n", webrtc_offer_object);

                    local_webrtc_data[room_id][senders_conn_id]['peer_conn_obj']
                            .setRemoteDescription(new RTCSessionDescription(webrtc_offer_object.offer));

                    local_webrtc_data[room_id][senders_conn_id]['peer_conn_obj']
                        .createAnswer().then(
                            _makeCreateAnswerHandler (
                                room_id,
                                parseInt(senders_conn_id),
                                parseInt(webrtc_offer_object.receivers_sig_serv_conn_id)
                            ),
                            _makeCreateAnswerErrorHandler(parseInt(senders_conn_id))
                        );
                
                    local_webrtc_data[room_id][senders_conn_id]['has_recieved_offer'] = true;
                }
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER:");
                
                var webrtc_answer_object = received_message_object.content;
                var room_id = webrtc_answer_object.room_id;
                var senders_conn_id = webrtc_answer_object.senders_sig_serv_conn_id;

                if( 
                    local_webrtc_data.hasOwnProperty(room_id)
                    && local_webrtc_data[room_id].hasOwnProperty(senders_conn_id)
                    && !(local_webrtc_data[room_id][senders_conn_id]['has_recieved_answer']) 
                ) {
                    _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER: Recieved Answer:\n", webrtc_answer_object);                

                    local_webrtc_data[room_id][senders_conn_id]['peer_conn_obj']
                            .setRemoteDescription(new RTCSessionDescription(webrtc_answer_object.answer));
                    
                    local_webrtc_data[room_id][senders_conn_id]['has_recieved_answer'] = true;
                }
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE:
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE:");
                
                var webrtc_candidate_object = received_message_object.content;
                var room_id = webrtc_candidate_object.room_id;
                var senders_conn_id = webrtc_candidate_object.senders_sig_serv_conn_id;
                
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE: Recieved Candidate:\n", webrtc_candidate_object);
                
                if (
                    local_webrtc_data.hasOwnProperty(room_id)
                    && local_webrtc_data[room_id].hasOwnProperty(senders_conn_id)
                ) {
                    // add ice candidate to the peer connection object associated with
                    // client that sent the LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
                    // message
                    local_webrtc_data[room_id][senders_conn_id]['peer_conn_obj']
                            .addIceCandidate(new RTCIceCandidate(webrtc_candidate_object.candidate));
                }
                break;

            default:
                _log2Console("case default: Error: Unknown Message Type");
                // write error code
                break;
        }
    }
};

/**
 * Returns a function that handles what happens after 
 * RTCPeerConnection.createOffer(offerOptions) has been successfully 
 * called without any error(s) occuring. The returned function sends 
 * the created offer to the intended participant via the signaling server.
 * 
 * @param {String|int} room_id    identifier used to get at the webrtc data 
 *                                for the room containing the participant 
 *                                whom an answer is being sent to.
 * @param {int} senders_conn_id   connection id of client that sent the offer
 *                                that an answer is being created for. will
 *                                be used to get at the webrtc data for the
 *                                client.
 * @param {int} receivers_conn_id connection id of client that recieved the 
 *                                offer that an answer is being created for.
 *                             
 * @returns {Function}
 */
function _makeCreateAnswerHandler (room_id, senders_conn_id, receivers_conn_id) {
    
    _log2Console("_makeCreateAnswerHandler('"+ room_id + "', " + senders_conn_id + ", " + receivers_conn_id + ")");
    
    return function(answer) {
        
        _log2Console('createAnswer Handler for room: ', room_id, ' senders_conn_id: ', senders_conn_id, ' and receivers_conn_id: ', receivers_conn_id, " about to >>>>setLocalDescription<<<< with created answer: \n", answer);
        
        local_webrtc_data[room_id][senders_conn_id]['peer_conn_obj'].setLocalDescription(answer);

        _log2Console('createAnswer Handler for room: ', room_id, ' senders_conn_id: ', senders_conn_id, ' and receivers_conn_id: ', receivers_conn_id, " >>>>SENDING<<<< created answer: \n", answer);

        // Send the answer to the remote peer through the signaling server.
        _sendToServer(
            connection,
            {
                "answer": answer,
                room_id: room_id,
                senders_sig_serv_conn_id: receivers_conn_id,
                receivers_sig_serv_conn_id: senders_conn_id
            },
            LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER
        );
        
        _log2Console('createAnswer Handler for room: ', room_id, ' senders_conn_id: ', senders_conn_id, ' and receivers_conn_id: ', receivers_conn_id, " >>>>SENT<<<< created answer: \n", answer);
    };
}

/**
 * Returns a function that handles what happens after 
 * RTCPeerConnection.createAnswer() has been called and one or more 
 * error(s) were generated. The returned function displays an error 
 * message (it also logs the message to the console).
 * 
 * @param {int} senders_conn_id can be used to get at the webrtc data for 
 *                              the participant whom an offer was sent to.
 *                                
 * @returns {Function}
 */
function _makeCreateAnswerErrorHandler(senders_conn_id) {
    
    _log2Console("_makeCreateAnswerErrorHandler("+ senders_conn_id + ")");
    
    return function(reason) {
        
        alert('in Create Answer Error Handler: Error Occurred When Creating Answer to Transmit Video to Conn `' + senders_conn_id + '`.');
        _log2Console('in Create Answer Error Handler: Error Occurred When Creating Answer to Transmit Video to Conn `' + senders_conn_id + '` :', reason);
    };
}

/**
 * Search for a participant with given connection id in the given room and
 * return the participant's name if the participant was found, else -1 
 * if not found.
 * 
 * @param {Object} meeting_room a meeting room object
 * @param {String} conn_id connection id of a participant to be searched for
 * 
 * @returns {Boolean} participant's name if the participant was found in the 
 *                    given room, else -1.
 */
function _getParticipantNameViaSigServConnId(meeting_room, conn_id) {
    
    var index_of_participant_to_find = meeting_room.participants.findIndex(

        function( participant ) {
            return parseInt(participant.sig_serv_conn_id) === parseInt(conn_id);
        }
    );
    
    return (index_of_participant_to_find !== -1) 
            ? meeting_room.participants[index_of_participant_to_find].participant_name 
            : -1;
}

/**
 * Search for a participant with given connection id in the given room.
 * 
 * @param {Object} meeting_room a meeting room object
 * @param {String} conn_id connection id of a participant to be searched for
 * 
 * @returns {Boolean} true if participant with specified id exists in the given
 *                    room, else false.
 */
function _participantWithSigServConnIdExists(meeting_room, conn_id) {
    
    var index_of_participant_to_find = meeting_room.participants.findIndex(

        function( participant ) {
            return parseInt(participant.sig_serv_conn_id) === parseInt(conn_id);
        }
    );
    
    return index_of_participant_to_find !== -1;
}

/**
 * Search for a participant with given name in the given room.
 * 
 * @param {Object} meeting_room a meeting room object
 * @param {String} participant_name name of a participant to be searched for
 * 
 * @returns {Boolean} true if participant with specified name exists in the given
 *                    room, else false.
 */
function _participantWithNameExists(meeting_room, participant_name) {
    
    var index_of_participant_to_find = meeting_room.participants.findIndex(

        function( participant ) {
            return participant.participant_name === participant_name;
        }
    );
    
    return index_of_participant_to_find !== -1; 
}

/**
 * Calculates the current date & time & returns it in YYYY-Mmm-dd hh:mm:ss format.
 * 
 * @returns {String} current date & time in YYYY-Mmm-dd hh:mm:ss format.
 */
function _getCurrentTimeStamp() {

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();
    var seconds = d.getSeconds();
    var minutes = d.getMinutes();
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    seconds = (seconds < 10) ? '0' + seconds : seconds;
    minutes = (minutes < 10) ? '0' + minutes : minutes;

    return year + '-' + months[month] + '-' + day + ' ' 
           + d.getHours() + ':' + minutes + ':' + seconds;
}

/**
 * Convert a message string to a javascript Object if the message is in JSON 
 * format.
 * 
 * @param {String} message received message to be parsed
 * 
 * @returns {String} return a javascript object if message is a string in JSON 
 *                   format, else return message as is if the message is a 
 *                   string that isn't in JSON format.
 */
function _parseRecievedMsg(message) {
    
    var parsed_msg = '';
    
    try { parsed_msg = JSON.parse(message); } 
    
    catch (e) { parsed_msg = message; }
    
    return parsed_msg;
}

/**
 * Helper function to send a message to the websocket server.
 * 
 * @param {WebSocket} conn (see https://developer.mozilla.org/en-US/docs/Web/API/WebSocket).
 * @param {mixed} message a javascript value to be sent in JSON format to the websocket server.
 * @param {String} type a value corresponding to any of the properties of the 
 *                 LAN_COMMUNICATOR_MSG_TYPES object.
 * 
 * @returns {void}
 */
function _sendToServer(conn, message, type) {
    
    // default type value to LAN_COMMUNICATOR_MSG_TYPES.GENERIC
    var final_type = LAN_COMMUNICATOR_MSG_TYPES.GENERIC;

    if ( arguments.length === 3 ) {
        
        // user called this function with a value for type
        final_type = type; 
    }

    conn.send(
            JSON.stringify( 
                    {
                        'content': message, 
                        'type': final_type, 
                        'time_sent': _getCurrentTimeStamp()
                    }
                ) 
        );
}

/**
 * Wrapper for console.log that adds timestamp to each print out.
 * 
 * Expects 1 or more arguments (each can be of any data-type). 
 * Calling this function with no arguments does nothing.
 * 
 * @returns {void}
 */
function _log2Console() {

    if ( arguments.length < 1 ) { return; } // nothing to do no arguments supplied

    var timestamp = '[' + _getCurrentTimeStamp() + ']';

    if ( arguments.length === 1 ) {

        ////////////////////////
        //one argument supplied
        ////////////////////////
        console.log(timestamp, arguments[0]);

    } else if ( arguments.length > 1 ) {

        //////////////////////////////////
        //more than one argument supplied
        //////////////////////////////////

        //convert arguments object to an array
        var args_as_array = Array.prototype.slice.call(arguments);

        //convert 1st arg to string prepended with %s
        args_as_array[0] = '%s ' + String(args_as_array[0]);

        //make timestamp the 2nd argument & shift other elements 1 position down
        var rearranged_args = [args_as_array.shift(), timestamp].concat(args_as_array);

        //call console.log with the modified arguments
        console.log.apply(console, rearranged_args);
    }
}

/**
 * Courtesy of http://locutus.io/php/var/is_object/
 * 
 * Tests if the supplied value contained in mixedVar is an Object
 * 
 * @param {mixed} mixedVar variable to test
 * 
 * @returns {Boolean} true if mixedVar is an object (and not an Array object) else false
 */
function _isObject (mixedVar) {

    if (Object.prototype.toString.call(mixedVar) === '[object Array]') { return false; }
    
    return (mixedVar !== null) && (typeof mixedVar === 'object');
}

/**
 * Tests if the specified object has any properties.
 * 
 * @param {Object} myObject object to test.
 * 
 * @returns {Boolean} true if any property exists, else false.
 */
function _objectHasAnyProperty(myObject) {
    
    return Object.keys(myObject).length > 0;
}
