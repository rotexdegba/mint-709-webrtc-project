"use strict";

/**
 *  Student ID: 011273515
 *  Name: Adedurotimi Adegbamigbe
 *  Course: MINT 709 Capstone Project
 *  
 *  This is a signaling server for a WebRTC web-application that allows 
 *  video-conferencing sessions between 2 to a maximum of 4 clients.
 * 
 */
var os = require('os');                     // Import the operating system package.
var fs = require('fs');                     // Import the file system package.
var httpServ = require('https');            // Import the https package.
var WebSocketServer = require('ws').Server; // Import the Websocket Server Class.

var LAN_COMMUNICATOR_MSG_TYPES = {
                         "INITIALIZE_M_ROOMS" : 'init-m-rooms',
    "TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR" : 'tag-client-websockconn-with-id-from-svr',
                              "CREATE_M_ROOM" : 'create-m-room',
                  "ADD_NEW_ROOM_TO_CLIENT_UI" : 'add-new-room-to-client-ui',
                                 "LEAVE_ROOM" : 'leave-room',
              "UPDATE_LEFT_ROOM_ON_CLIENT_UI" : 'update-left-room-on-client-ui',
                        "DELETE_ROOM_FROM_UI" : 'delete-room-from-ui',
                                  "JOIN_ROOM" : 'join-room',
            "UPDATE_JOINED_ROOM_ON_CLIENT_UI" : 'update-joined-room-on-client-ui',
                               "WEBRTC_OFFER" : 'webrtc-offer',
                              "WEBRTC_ANSWER" : 'webrtc-answer',
                           "WEBRTC_CANDIDATE" : 'webrtc-candidate',
                                    "GENERIC" : 'generic'
};

var MAX_NUM_PARTICIPANTS_IN_ROOM = 4;

var cfg = {
    ssl: true,
    port: 8888,
    ssl_key: 'key.pem',
    ssl_cert: 'cert.pem'
}; // SSL configuration values for wss (secure websockets).

// dummy request processing
var processRequest = function (req, res) {

    res.writeHead(200);
    res.end("Supercalifragilistic WebSockets!\n");
};

// provide server with SSL key/cert
var ssl_opts = {
    key: fs.readFileSync(cfg.ssl_key),
    cert: fs.readFileSync(cfg.ssl_cert)
};

var https_webserver_object = httpServ.createServer(ssl_opts, processRequest).listen(cfg.port);

//var wss = new WebSocketServer({port : 8888});
var wss = new WebSocketServer({server: https_webserver_object});// Create an instance of the
                                                                // Websocket Server Class which 
                                                                // will listen on port 8888 for 
                                                                // clients trying to establish a 
                                                                // websocket connection with this 
                                                                // server. The https web server 
                                                                // object is passed to the 
                                                                // WebSocketServer constructor 
                                                                // so WS would know the port & 
                                                                // SSL capabilities.
var meeting_rooms = {}; // An object that keeps track of all 
                        // meeting sessions between clients 
                        // connected to this server. Start 
                        // with no meeting rooms.
                                    
var connected_clients = {}; // An object that keeps track of all existing
                            // client websocket connections to this server.
                                    
var next_connection_id = 1; // A global integer counter that will be used to
                            // assign a unique connection id to each new 
                            // WebSocket connection made to this server.
                                    
var next_meeting_room_id = 1; // A global integer counter that will be used to
                              // assign a unique room_id to each new 
                              // WebSocket connection made to this server.

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Start: Setting up handlers for the events associated with the wss object.
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// NOTE TO SELF: SEEMS LIKE wss.on('headers', wss.on('listening' and 
// wss.on('connection' are the events that matter most and so far
// are the only events I have seen triggered for the wss object.

wss.on('error', webSockSvrErrorHandler);         // Fired when this server (which 
                                                 // is really an instance of ws.Server; 
                                                 // a.k.a WebSocketServer) emits an error.

wss.on('headers', webSockSvrHeadersHandler);     // Fired when a client is in  
                                                 // the process of establishing 
                                                 // a connection to this server
                                                 // (which is really an instance
                                                 // of ws.Server; a.k.a WebSocketServer).

wss.on('listening', webSockSvrListeningHandler); // Fired once when this server (which
                                                 // is really an instance of ws.Server; 
                                                 // a.k.a WebSocketServer) is started 
                                                 // & listening on the port specified 
                                                 // in cfg.port.

// Below, the handler is being setup for the "onconnection" event, which gets 
// fired when a establishes a new WebSocket connection to this server (which 
// is really an instance of ws.Server; a.k.a WebSocketServer).
// 
// For example a javascript script running in a browser could access this server 
// via the WebSocket API in most modern browsers via the line of code below:
// 
//      var connection = new WebSocket('wss://x.x.x.x:8888'); // Where x.x.x.x is
// the IP address of
// the machine this
// server is running
// on.
wss.on('connection', webSockSvrConnectionHandler);

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
// End: Setting up handlers for the events associated with the wss object.
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Start: Defining functions to handle events associated with the wss object.
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

/**
 * Handles the "onconnection" event triggered when a client establishes a new 
 * WebSocket connection to this server (which is an instance of ws.Server;
 * a.k.a WebSocketServer).
 * 
 * @param {ws.WebSocket} socket_connection (see https://github.com/websockets/ws/blob/master/doc/ws.md#class-wswebsocket).
 * 
 * @returns {void}
 */
function webSockSvrConnectionHandler(socket_connection) {

    //_log2Console(Object.keys(socket_connection.upgradeReq));
    //_log2Console("webSockSvrConnectionHandler(socket_connection): Arguments %s :", arguments.length, arguments);
    _log2Console("webSockSvrConnectionHandler(socket_connection): User with IP address '", socket_connection.upgradeReq.connection.remoteAddress, "' connected.");
    _log2File("webSockSvrConnectionHandler(socket_connection): User with IP address '" + socket_connection.upgradeReq.connection.remoteAddress + "' connected.");

    if( !socket_connection.hasOwnProperty('my_ip_addy') ) {
        
        _log2Console("webSockSvrConnectionHandler(socket_connection): assigning my_ip_addy value of `%s` to the connection.", socket_connection.upgradeReq.connection.remoteAddress);
        
        // store the ip address of the newly connected client (accessible 
        // via the `.upgradeReq.connection.remoteAddress` property of the 
        // Websocket Connection object) in a new property on the 
        // Websocket Connection object called `my_ip_addy`.
        socket_connection.my_ip_addy = socket_connection.upgradeReq.connection.remoteAddress;
    }

    if( !socket_connection.hasOwnProperty('sig_serv_conn_id') ) {

        _log2Console("webSockSvrConnectionHandler(socket_connection): assigning sig_serv_conn_id value of `%d` to the connection from User with IP address` %s` . ", next_connection_id, socket_connection.upgradeReq.connection.remoteAddress);

        // assign a unique id and store this unique id in a new property
        // on the Websocket Connection object called `sig_serv_conn_id`.
        socket_connection.sig_serv_conn_id = next_connection_id++;
                
        if( !connected_clients.hasOwnProperty(socket_connection.sig_serv_conn_id) ) {
            
            // add the client's Websocket Connection object to the 
            // connected_clients object with a property / key name 
            // whose value is the just assigned `sig_serv_conn_id`.
            connected_clients[socket_connection.sig_serv_conn_id] = socket_connection;
        }
    }
    
    //_log2Console("webSockSvrConnectionHandler(socket_connection): connection object after property assignments: ", socket_connection);
    //_log2Console("webSockSvrConnectionHandler(socket_connection): connected_clients object after property assignments: ", connected_clients);

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////
    // Start: Setting up handlers for the events associated with the socket_connection object.
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////

    // NOTE TO SELF: SEEMS LIKE socket_connection.on('close' and 
    // socket_connection.on('message' are the events that matter 
    // most and so far are the only events I have seen triggered 
    // for the socket_connection object.

    // Setup handler for the "onopen" event. 
    // This seems to get fired when using the 'ws' package to write a client 
    // application. This event will most likely never be fired since this is
    // a server script.
    socket_connection.on('open', webSockConnOnOpenHandler);

    // Setup handler for the "onerror" event which gets fired when an error
    // occurs on a connection between a client and this server. 
    socket_connection.on('error', webSockConnOnErrorHandler);

    // Setup handler for the "onclose" event which gets fired when a client 
    // disconnects from this server.
    socket_connection.on('close', webSockConnOnCloseHandler);

    // Setup handler for the "onmessage" event which gets fired when a client 
    // sends a message to this server. 
    socket_connection.on('message', webSockConnOnMsgHandler);

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    // End: Setting up handlers for the events associated with the socket_connection object.
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    // Send messages back to the client, confirming that a 
    // websocket connection has been established with the client.
    
    // Send the server generated socket id (ie. socket_connection.sig_serv_conn_id)
    // back to the client so that the client can add the value to its WebSocket object.
    _sendToClient(
        socket_connection,
        { "sig_serv_conn_id" : socket_connection.sig_serv_conn_id },
        LAN_COMMUNICATOR_MSG_TYPES.TAG_CLIENT_WEBSOCKCONN_WITH_ID_FROM_SVR
    );
    
    // Send rooms object to client, so that the client can update its
    // user-interface.
    _sendToClient(
        socket_connection,
        meeting_rooms,
        LAN_COMMUNICATOR_MSG_TYPES.INITIALIZE_M_ROOMS
    );
}

/**
 * Handles the "onerrors" event on an instance of ws.Server 
 * (a.k.a WebSocketServer) which occurs if the server emits 
 * an error.
 * 
 * @param {Object} error an object containing error information.
 * 
 * @returns {void}
 */
function webSockSvrErrorHandler(error) {

    _log2File("webSockSvrErrorHandler(error): Error: Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    _log2Console("webSockSvrErrorHandler(error): Error: Args length: ", arguments.length, ' Arguments:', arguments);
}

/**
 * Handles the "onheaders" event on an instance of ws.Server 
 * (a.k.a WebSocketServer) which occurs when a client is in 
 * the process of establishing a connection to this server.
 * 
 * @param {Array} headers an array of HTTP header strings for upgrading from http(s) 
 *                        to the Websockets protocol (ws / wss). 
 *                        For example:
 *                          [
 *                              'HTTP/1.1 101 Switching Protocols',
 *                              'Upgrade: websocket',
 *                              'Connection: Upgrade',
 *                              'Sec-WebSocket-Accept: KO61N8XUhQAXZGlVNXJkDa2fZt4=',
 *                              'Sec-WebSocket-Extensions: permessage-deflate'
 *                          ]
 * @returns {void}
 */
function webSockSvrHeadersHandler(headers) {

    _log2Console("webSockSvrHeadersHandler(headers): See log file for headers.");
    _log2File("webSockSvrHeadersHandler(headers): headers: " + os.EOL + "\t[" + os.EOL + "\t\t" + (headers.join(os.EOL + "\t\t")) + os.EOL + "\t]");
    
    _log2File("webSockSvrHeadersHandler(headers): Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    _log2Console("webSockSvrHeadersHandler(headers): Args length: ", arguments.length, ' Arguments:', arguments);
}

/**
 * Handles the "onlistening" event which occurs once when an instance 
 * of ws.Server (a.k.a WebSocketServer) is created & listening on the 
 * port specified in cfg.port.
 * 
 * @returns {void}
 */
function webSockSvrListeningHandler() {

    _log2Console("webSockSvrListeningHandler(): Signaling Server started...");
    _log2File("webSockSvrListeningHandler(): Signaling Server started...");

    //_log2File("webSockSvrListeningHandler(headers): Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    //_log2Console("webSockSvrListeningHandler(headers): Args length: ", arguments.length, ' Arguments:', arguments);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// End: Defining functions to handle events associated with the wss object.
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
// Start: Defining functions to handle events associated with the socket_connection 
//        object passed to webSockSvrConnectionHandler.
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

/**
 * Handler for the "onopen" event on an instance of ws.WebSocket.
 * 
 * THIS HANDLER WILL MOST LIKELY NEVER GET CALLED SINCE THIS IS A SERVER SCRIPT.
 * SEEMS LIKE ws.WebSocket.onopen GETS FIRED WHEN USING THE ws PACKAGE TO WRITE
 * A CLIENT SCRIPT THAT CONNECTS TO A WebSocket SERVER.
 * 
 * @param {Event} on_open_event
 * 
 * @returns {void}
 */
function webSockConnOnOpenHandler(on_open_event) {

    _log2File("webSockConnOnOpenHandler(on_open_event): Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    _log2Console("webSockConnOnOpenHandler(on_open_event): Args length: ", arguments.length, ' Arguments:', arguments);
}

/**
 * Handler for the "onerror" event on an instance of ws.WebSocket.
 *  
 * @param {Event} on_error_event
 * 
 * @returns {void}
 */
function webSockConnOnErrorHandler(on_error_event) {

    _log2File("webSockConnOnErrorHandler(on_error_event): Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    _log2Console("webSockConnOnErrorHandler(on_error_event): Args length: ", arguments.length, ' Arguments:', arguments);
}

/**
 * Handler for the "onclose" event on an instance of ws.WebSocket.
 * 
 * @param {int} exit_code an integer value. See https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent
 *                        for a list of standard / permitted exit / close codes.
 *  
 * =============================================================================
 * | Status code  | Name                 | Description
 * =============================================================================
 * | 0–999        |                      | Reserved and not used.
 * | 1000         | CLOSE_NORMAL         | Normal closure; the connection successfully completed whatever purpose for which it was created.
 * | 1001         | CLOSE_GOING_AWAY     | The endpoint is going away, either because of a server failure or because the browser is navigating away from the page that opened the connection.
 * | 1002         | CLOSE_PROTOCOL_ERROR | The endpoint is terminating the connection due to a protocol error.
 * | 1003         | CLOSE_UNSUPPORTED    | The connection is being terminated because the endpoint received data of a type it cannot accept (for example, a text-only endpoint received binary data).
 * | 1004         |                      | Reserved. A meaning might be defined in the future.
 * | 1005         | CLOSE_NO_STATUS      | Reserved.  Indicates that no status code was provided even though one was expected.
 * | 1006         | CLOSE_ABNORMAL       | Reserved. Used to indicate that a connection was closed abnormally (that is, with no close frame being sent) when a status code is expected.
 * | 1007         | Unsupported Data     | The endpoint is terminating the connection because a message was received that contained inconsistent data (e.g., non-UTF-8 data within a text message).
 * | 1008         | Policy Violation     | The endpoint is terminating the connection because it received a message that violates its policy. This is a generic status code, used when codes 1003 and 1009 are not suitable.
 * | 1009         | CLOSE_TOO_LARGE      | The endpoint is terminating the connection because a data frame was received that is too large.
 * | 1010         | Missing Extension    | The client is terminating the connection because it expected the server to negotiate one or more extension, but the server didn't.
 * | 1011         | Internal Error       | The server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.
 * | 1012         | Service Restart      | The server is terminating the connection because it is restarting. [Ref]
 * | 1013         | Try Again Later      | The server is terminating the connection due to a temporary condition, e.g. it is overloaded and is casting off some of its clients. [Ref]
 * | 1014         |                      | Reserved for future use by the WebSocket standard.
 * | 1015         | TLS Handshake        | Reserved. Indicates that the connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).
 * | 1016–1999    |                      | Reserved for future use by the WebSocket standard.
 * | 2000–2999    |                      | Reserved for use by WebSocket extensions.
 * | 3000–3999    |                      | Available for use by libraries and frameworks. May not be used by applications. Available for registration at the IANA via first-come, first-serve.
 * | 4000–4999    |                      | Available for use by applications.
 * =============================================================================
 *                          
 * @param {String} exit_reason the reason the server closed the connection.
 * 
 * @returns {void}
 */
function webSockConnOnCloseHandler(exit_code, exit_reason) {

    // FOR FUTURE ENHANCEMENT: wss.clients WILL AT THIS POINT NO LONGER CONTAIN
    //                         THE CURRENT WEBSOCKET OBJECT THAT IS BEING CLOSED.
    //                         HOWEVER IT CAN STILL BE ACCESSED VIA `this` .
    
    _log2Console("webSockConnOnCloseHandler(exit_code, exit_reason): Closing Connection: ", this.sig_serv_conn_id);
    _log2File("webSockConnOnCloseHandler(exit_code, exit_reason): Closing Connection: " + this.sig_serv_conn_id);

    _log2Console("webSockConnOnCloseHandler(exit_code, exit_reason): Exit Code: %s and Exit Reason: %s ", exit_code, exit_reason);
    _log2File("webSockConnOnCloseHandler(exit_code, exit_reason): " + exit_code + ' and Exit Reason: ' + exit_reason);

    _log2File("webSockConnOnCloseHandler(exit_code, exit_reason): Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    _log2Console("webSockConnOnCloseHandler(exit_code, exit_reason): Args length: ", arguments.length, ' Arguments:', arguments);

    _log2Console("webSockConnOnCloseHandler(exit_code, exit_reason): connection object in webSockConnOnCloseHandler(exit_code, exit_reason) before removing it from connected_clients: ", this);
    _log2Console("webSockConnOnCloseHandler(exit_code, exit_reason): connected_clients object in webSockConnOnCloseHandler(exit_code, exit_reason) before removing closing websocket: ", connected_clients);
    
    var room_ids_for_deletion = [];
    
    for (var room_id in meeting_rooms) {
        
        // search list of the current room's participants to see if the
        // websocket connection being closed is associated with any of 
        // the participants.
        var index_of_participant_to_remove = meeting_rooms[room_id].participants.findIndex(
                            
            function( participant ) {
                return participant.sig_serv_conn_id === this.sig_serv_conn_id;
            },
            this
        );

        if( index_of_participant_to_remove !== -1 ) {
            
            _log2Console('webSockConnOnCloseHandler(exit_code, exit_reason): meeting_rooms before delete: ', meeting_rooms);
            _log2Console('webSockConnOnCloseHandler(exit_code, exit_reason): participants before splice: ', meeting_rooms[room_id].participants);
            _log2Console('webSockConnOnCloseHandler(exit_code, exit_reason): index_of_participant_to_leave: ', index_of_participant_to_remove);

            // a participant associated with the connection being 
            // closed was found. Delete the participant from the room.
            meeting_rooms[room_id].participants.splice(index_of_participant_to_remove, 1);

            _log2Console('webSockConnOnCloseHandler(exit_code, exit_reason): participants after splice: ', meeting_rooms[room_id].participants);

            if( meeting_rooms[room_id].participants.length <= 0 ) {

                // the deleted participant was the last participant
                // in the meeting room. Since we can't have a meeting
                // room without any participant, we have to delete the
                // meeting room from the meeting_rooms object.
                room_ids_for_deletion.push(room_id);

            } else {

                // send back the updated meeting room object to other connected 
                // clients since there are still one or more participant(s) in 
                // meeting_rooms[leave_room_message_object.room_id].participants
                // Send the message with a type value of 
                // LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI.
                for (var sig_serv_conn_id in connected_clients) {

                    if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) ) {

                        _sendToClient(
                            connected_clients[sig_serv_conn_id],
                            { 
                                "updated_meeting_room" : meeting_rooms[room_id],
                                "leavers_sig_serv_conn_id" : this.sig_serv_conn_id
                            },
                            LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI
                        );
                    } // if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) )
                } // for (var sig_serv_conn_id in connected_clients)
            } // if( meeting_rooms[room_id].participants.length <= 0 ) ... else
        } // if( index_of_participant_to_remove !== -1 )
    } // for (var room_id in meeting_rooms)

    // delete the rooms marked for deletion because 
    // they no longer have participant(s) in them.
    room_ids_for_deletion.forEach(
                            
        function( room_id ) {
            
            delete meeting_rooms[room_id];
            
            // send message back to other connected clients to delete room 
            // from their user-interfaces (UIs).
            for (var sig_serv_conn_id in connected_clients) {
        
                if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) ) {
                    
                    _sendToClient(
                        connected_clients[sig_serv_conn_id],
                        { "room_id" : room_id },
                        LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI
                    );
                } // if( parseInt(this.sig_serv_conn_id) !== parseInt(sig_serv_conn_id) )
            } // for (var sig_serv_conn_id in connected_clients)
        },
        this
    );
    
    _log2Console('webSockConnOnCloseHandler(exit_code, exit_reason): meeting_rooms after delete: ', meeting_rooms);
                
    if( connected_clients.hasOwnProperty(this.sig_serv_conn_id) ) {
        
        // remove the client's Websocket Connection 
        // object from the connected_clients object.
        delete connected_clients[this.sig_serv_conn_id];
        
    } // if( connected_clients.hasOwnProperty(this.sig_serv_conn_id) )
    
    _log2Console("webSockConnOnCloseHandler(exit_code, exit_reason): connected_clients object after removing closing websocket: ", connected_clients);
}

/**
 * Handler for the "onmessage" event on an instance of ws.WebSocket.
 * 
 * @param {String} message the message received from a client.
 * @param {Object} flags contains some meta-data.
 * 
 * @returns {void}
 */
function webSockConnOnMsgHandler(message, flags) {

    var received_message_object = _parseRecievedMsg(message);
    
    _log2File("webSockConnOnMsgHandler(message, flags): Args length: " + arguments.length + ' Arguments:' + os.EOL + JSON.stringify(arguments));
    _log2Console("webSockConnOnMsgHandler(message, flags): Args length: ", arguments.length, ' Arguments:', arguments);
    
    _log2Console("webSockConnOnMsgHandler(message, flags): Got message: ", message, " from client ", this.sig_serv_conn_id);
    _log2Console("webSockConnOnMsgHandler(message, flags): Parsed message: ", received_message_object, " from client", this.sig_serv_conn_id);
    
    _log2File('webSockConnOnMsgHandler(message, flags): Got message: ' + os.EOL + JSON.stringify(message) + os.EOL + ' from client ' + this.sig_serv_conn_id );
    _log2File('webSockConnOnMsgHandler(message, flags): Parsed message: ' + os.EOL + JSON.stringify(received_message_object) + os.EOL + ' from client ' + this.sig_serv_conn_id );

    
    if( _isObject(received_message_object) ) {
        
        switch (received_message_object.type) {

            case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM:
                
                //get the data sent from client that will be used to create the
                //new meeting room
                var new_meeting_room_data = received_message_object.content;
                _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM: new_meeting_room_data.room_name: ', new_meeting_room_data.room_name);
                _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM: new_meeting_room_data.creators_name: ', new_meeting_room_data.creators_name);
                _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM: sig_serv_conn_id: ', this.sig_serv_conn_id);
                
                var new_room_object = {};
                new_room_object['room_id'] = next_meeting_room_id++;
                new_room_object['room_name'] = new_meeting_room_data.room_name;
                new_room_object['participants'] = [];
                
                var creators_participant_obj = {};
                creators_participant_obj['sig_serv_conn_id'] = this.sig_serv_conn_id;
                creators_participant_obj['participant_name'] = new_meeting_room_data.creators_name;
                creators_participant_obj['is_creator'] = true;
                
                //add the creator's participant object to the participants array
                //of the new room object
                new_room_object['participants'].push(creators_participant_obj);
                
                //add new meeting room to the global meeting_rooms object
                meeting_rooms[new_room_object.room_id] = new_room_object;
                _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM: new_room_object: ', new_room_object);
                _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.CREATE_M_ROOM: meeting_rooms: ', meeting_rooms);
                
                //send message back to all connected clients to each update
                //their UI with new meeting room data       
                for (var sig_serv_conn_id in connected_clients) {
                    
                    _sendToClient(
                        connected_clients[sig_serv_conn_id],
                        { 'new_room_object' : new_room_object, 'creators_sig_serv_conn_id': this.sig_serv_conn_id },
                        LAN_COMMUNICATOR_MSG_TYPES.ADD_NEW_ROOM_TO_CLIENT_UI
                    );
                }
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM:
                
                var leave_room_message_object = received_message_object.content;
                
                if ( meeting_rooms.hasOwnProperty(leave_room_message_object.room_id) ) {
                    
                    // a room with the sent meeting room id exists
                    var meeting_room_to_leave = meeting_rooms[leave_room_message_object.room_id];
                    
                    //search for the participant with the sent name
                    var index_of_participant_to_leave = meeting_room_to_leave.participants.findIndex(
                            
                        function( participant ) {
                            return participant.participant_name === leave_room_message_object.participant_name;
                        }
                    );
            
                    if( index_of_participant_to_leave !== -1 ) {
                        
                        _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: meeting_rooms before delete: ', meeting_rooms);
                        _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: participants before splice: ', meeting_rooms[leave_room_message_object.room_id].participants);
                        _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: index_of_participant_to_leave: ', index_of_participant_to_leave);
                        
                        // the participant to leave was found
                        // delete the participant from the room
                        meeting_rooms[leave_room_message_object.room_id].participants.splice(index_of_participant_to_leave, 1);
                        _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: participants after splice: ', meeting_rooms[leave_room_message_object.room_id].participants);
                        
                        if( meeting_rooms[leave_room_message_object.room_id].participants.length <= 0 ) {
                            
                            // the deleted participant was the last participant
                            // in the meeting room. since we can't have a meeting
                            // room without any participant, we have to delete the
                            // meeting room from the meeting_rooms object.
                            delete meeting_rooms[leave_room_message_object.room_id];
                            _log2Console('webSockConnOnMsgHandler(message, flags): case LAN_COMMUNICATOR_MSG_TYPES.LEAVE_ROOM: meeting_rooms after delete: ', meeting_rooms);
                    
                            // send message back to all connected clients to delete room 
                            // from their user-interfaces (UIs).
                            for (var sig_serv_conn_id in connected_clients) {

                                _sendToClient(
                                    connected_clients[sig_serv_conn_id],
                                    { "room_id" : leave_room_message_object.room_id },
                                    LAN_COMMUNICATOR_MSG_TYPES.DELETE_ROOM_FROM_UI
                                );
                            }
                            
                        } else {
                            
                            // send back the updated meeting room object to all
                            // connected clients since there are still one or more participant(s) 
                            // in meeting_rooms[leave_room_message_object.room_id].participants
                            // Send the message with a type value of LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI.
                            for (var sig_serv_conn_id in connected_clients) {

                                _sendToClient(
                                    connected_clients[sig_serv_conn_id],
                                    { 
                                        "updated_meeting_room" : meeting_rooms[leave_room_message_object.room_id],
                                        "leavers_sig_serv_conn_id" : this.sig_serv_conn_id
                                    },
                                    LAN_COMMUNICATOR_MSG_TYPES.UPDATE_LEFT_ROOM_ON_CLIENT_UI
                                );
                            }
                        } // if( meeting_rooms[leave_room_message_object.room_id].participants.length <= 0 ) ... else
                    } else {
                        
                        // specified participant to leave room does
                        // not exists in room. Send message back to 
                        // the client to reflect this.
                        
                    } // if( index_of_participant_to_leave !== -1 )  ... else
                } else {
                    // a room with the sent meeting room id does not exist
                    
                    // send message back to the client that the desired  
                    // room to be left does not exist on the server.
                } //if ( meeting_rooms.hasOwnProperty(leave_room_message_object.room_id) ) ... else               
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.JOIN_ROOM:
                
                var join_room_message_object = received_message_object.content;
                
                if ( meeting_rooms.hasOwnProperty(join_room_message_object.room_id) ) {
                    
                    //////////////////////////////////////////////
                    // a room with the sent meeting room id exists
                    //////////////////////////////////////////////
                    //add a new participant
                    var participant_obj = {};
                    participant_obj['sig_serv_conn_id'] = this.sig_serv_conn_id;
                    participant_obj['participant_name'] = join_room_message_object.participant_name;
                    participant_obj['is_creator'] = false;

                    //add the participant object to the participants array
                    //of the room object
                    meeting_rooms[join_room_message_object.room_id].participants.push(participant_obj);
            
                    // send back the updated meeting room object to all
                    // connected clients.
                    // Send the message with a type value of 
                    // LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI.
                    for (var sig_serv_conn_id in connected_clients) {

                        _sendToClient(
                            connected_clients[sig_serv_conn_id],
                            { 
                                "updated_meeting_room" : meeting_rooms[join_room_message_object.room_id],
                                "joiners_sig_serv_conn_id" : this.sig_serv_conn_id
                            },
                            LAN_COMMUNICATOR_MSG_TYPES.UPDATE_JOINED_ROOM_ON_CLIENT_UI
                        );
                    }

                } else {
                    // a room with the sent meeting room id does not exist
                    // send message back to the client that the desired  
                    // room to be joined does not exist on the server.
                }
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER:
                
                var webrtc_offer_object = received_message_object.content;
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER: Recieved Offer:", os.EOL, webrtc_offer_object);
                
                //forward offer to specified client
                _sendToClient(
                    connected_clients[webrtc_offer_object.receivers_sig_serv_conn_id],
                    webrtc_offer_object,
                    LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_OFFER
                );
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER:
                
                var webrtc_answer_object = received_message_object.content;
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER: Recieved Answer:", os.EOL, webrtc_answer_object);
                
                //forward answer to specified client
                _sendToClient(
                    connected_clients[webrtc_answer_object.receivers_sig_serv_conn_id],
                    webrtc_answer_object,
                    LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_ANSWER
                );
                break;
                
            case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE:
                
                var webrtc_candidate_object = received_message_object.content;
                _log2Console("case LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE: Recieved Candidate:", os.EOL, webrtc_candidate_object);
                
                //forward candidate to specified client
                _sendToClient(
                    connected_clients[webrtc_candidate_object.receivers_sig_serv_conn_id],
                    webrtc_candidate_object,
                    LAN_COMMUNICATOR_MSG_TYPES.WEBRTC_CANDIDATE
                );
                break;

            default:
                // write error code
                break;
        } // switch (received_message_object.type)
    } // if( _isObject(received_message_object) )
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
// End: Defining functions to handle events associated with the socket_connection 
//      object passed to webSockSvrConnectionHandler
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
// Start: Defining helper functions for common tasks like writing to a log file, etc.
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Convert a message string to a javascript object if the message is in JSON 
 * format.
 * 
 * @param {String} message received message to be parsed
 * 
 * @returns {String} return a javascript object if message is a string in JSON 
 *                   format, else return message as is if message is a string 
 *                   that isn't in JSON format.
 */
function _parseRecievedMsg( message ) {

    var parsed_msg = '';

    try { parsed_msg = JSON.parse(message); } 
    catch (e) { parsed_msg = message; }

    return parsed_msg;
}

/**
 * Helper function to send a message to a client.
 * 
 * @param {ws.WebSocket} conn (see https://github.com/websockets/ws/blob/master/doc/ws.md#class-wswebsocket).
 * @param {mixed} message a javascript value to be sent in JSON format to a client.
 * @param {String} type a value corresponding to any of the properties of the 
 *                    LAN_COMMUNICATOR_MSG_TYPES object.
 * 
 * @returns {void}
 */
function _sendToClient( conn, message, type ) {

    // default type value to LAN_COMMUNICATOR_MSG_TYPES.GENERIC
    var final_type = LAN_COMMUNICATOR_MSG_TYPES.GENERIC;

    if ( arguments.length === 3 ) {
        
        // user called this function with a value for type
        final_type = type; 
    }

    conn.send(
            JSON.stringify(
                    {
                        'content': message, 
                        'type': final_type, 
                        'time_sent': _getCurrentTimeStamp()
                    }
                ) 
        );
}

/**
 * Helper function to write a string of text to a daily log file.
 * 
 * @param {String} message text to be written (appended) to the log file.
 * 
 * @returns {void}
 */
function _log2File( message ) {

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var line = '[' + _getCurrentTimeStamp() + '] [MESSAGE]: ' + message + os.EOL;

    var log_file_name = 'daily_log_' + year + '_' + months[month] + '_' + day + '.txt';

    _checkDirectorySync('./logs'); // check if directory exists
    fs.appendFileSync('./logs/' + log_file_name, line, {'mode': 0o664});
}

/**
 * Wrapper for console.log that adds timestamp to each print out.
 * 
 * Expects 1 or more arguments of any data-type. 
 * Calling this function with no arguments does nothing.
 * 
 * @returns {void}
 */
function _log2Console() {

    if ( arguments.length < 1 ) { return; } // nothing to do no arguments supplied

    var timestamp = '[' + _getCurrentTimeStamp() + ']';

    if ( arguments.length === 1 ) {

        ////////////////////////
        //one argument supplied
        ////////////////////////
        console.log(timestamp, arguments[0]);

    } else if ( arguments.length > 1 ) {

        //////////////////////////////////
        //more than one argument supplied
        //////////////////////////////////

        //convert arguments object to an array
        var args_as_array = Array.prototype.slice.call(arguments);

        //convert 1st arg to string prepended with %s
        args_as_array[0] = '%s ' + String(args_as_array[0]);

        //make timestamp the 2nd argument & shift other elements 1 position down
        var rearranged_args = [args_as_array.shift(), timestamp].concat(args_as_array);

        //call console.log with the modified arguments
        console.log.apply(null, rearranged_args);
    }
}

/**
 * Calculates the current date & time & returns it in YYYY-Mmm-dd hh:mm:ss format.
 * 
 * @returns {String} current date & time in YYYY-Mmm-dd hh:mm:ss format.
 */
function _getCurrentTimeStamp() {

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();
    var seconds = d.getSeconds();
    var minutes = d.getMinutes();
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    seconds = (seconds < 10) ? '0' + seconds : seconds;
    minutes = (minutes < 10) ? '0' + minutes : minutes;

    return year + '-' + months[month] + '-' + day + ' ' 
           + d.getHours() + ':' + minutes + ':' + seconds;
}

/**
 * Courtesy of http://locutus.io/php/var/is_object/
 * 
 * Tests if the supplied value contained in mixedVar is an Object
 * 
 * @param {mixed} mixedVar variable to test
 * 
 * @returns {Boolean} true if mixedVar is an object (and not an Array object) else false
 */
function _isObject ( mixedVar ) {

  if ( Object.prototype.toString.call(mixedVar) === '[object Array]' ) { return false; }
  
  return ( mixedVar !== null ) && ( typeof mixedVar === 'object' );
}

/**
 * Checks if a directory exists. If it doesn't exist, it creates it.
 * 
 * Courtesy of
 * https://blog.raananweber.com/2015/12/15/check-if-a-directory-exists-in-node-js/
 * 
 * @param {String} directory path to a directory.
 * 
 * @returns {void}
 */
function _checkDirectorySync( directory ) {

    try { fs.statSync(directory); } catch ( e ) { fs.mkdirSync(directory); }
}

/**
 // Async version of _checkDirectorySync courtesy 
 // https://blog.raananweber.com/2015/12/15/check-if-a-directory-exists-in-node-js/
 // function will check if a directory exists, and create it if it doesn't
 
 function _checkDirectory(directory, callback) {  
 fs.stat(directory, function(err, stats) {
 //Check if error defined and the error code is "not exists"
 if (err && err.errno === 34) {
 //Create the directory, call the callback.
 fs.mkdir(directory, callback);
 } else {
 //just in case there was a different error:
 callback(err)
 }
 });
 }
 
 //Usage is rather simple:
 _checkDirectory("./logs/", function(error) {  
 if(error) {
 console.log("oh no!!!", error);
 } else {
 //Carry on, all good, directory exists / created.
 }
 });
 
 */
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
// End: Defining helper functions for common tasks like writing to a log file, etc.
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
