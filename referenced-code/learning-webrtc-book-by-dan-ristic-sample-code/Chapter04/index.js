"use strict";

var os = require('os');                     // Import the operating system package.
var fs = require('fs');                     // Import the file system package.
var httpServ = require('https');            // Import the https package.
var WebSocketServer = require('ws').Server; // Import the Websocket Server Class.
var LAN_COMMUNICATOR_MSG_TYPES = {
    "CURRENT_STATUS": 'current-status',
    "GENERIC": 'generic'
};
var cfg = {
    ssl: true,
    port: 8888,
    ssl_key: './../../../actual-project-code/key.pem',
    ssl_cert: './../../../actual-project-code/cert.pem'
}; // SSL configuration values for wss (secure websockets).

// dummy request processing
var processRequest = function (req, res) {
    //_log2Console("in var processRequest = function (req, res) {: ", req, res);
    _log2Console("in var processRequest = function (req, res) {: ");
    res.writeHead(200);
    res.end("Supercalifragilistic WebSockets!\n");
};

// provide server with SSL key/cert
var ssl_opts = {
    key: fs.readFileSync(cfg.ssl_key),
    cert: fs.readFileSync(cfg.ssl_cert)
};

var https_webserver_object = httpServ.createServer(ssl_opts, processRequest).listen(cfg.port);

//var wss = new WebSocketServer({port : 8888});
var wss = new WebSocketServer({server: https_webserver_object});// Create an instance of the
                                                                // Websocket Server Class which 
                                                                // will listen on port 8888 for 
                                                                // clients trying to establish a 
                                                                // websocket connection with this 
                                                                // server. The https web server 
                                                                // object is passed to the 
                                                                // WebSocketServer constructor 
                                                                // so WS would know the port & 
                                                                // SSL capabilities.




////////////////////////////////////////////////////////////////////////////////
var // WebSocketServer = require('ws').Server,
    // wss = new WebSocketServer({ port: 8888 }),
    users = {};

wss.on('connection', function (connection) {
  
  //_log2Console("in wss.on('connection', function (connection) {: ", connection);
  _log2Console("in wss.on('connection', function (connection) {: ");
  
  connection.on('message', function (message) {
    
    //_log2Console("in wss.on('connection', function (connection) { connection.on('message', function (message) { : ", message);
    _log2Console("in wss.on('connection', function (connection) { connection.on('message', function (message) { : ");
    var data;

    try {
      data = JSON.parse(message);
    } catch (e) {
      //_log2Console("in wss.on('connection', function (connection) { connection.on('message', function (message) { try {} catch(e) {... Error parsing JSON", e);
      _log2Console("in wss.on('connection', function (connection) { connection.on('message', function (message) { try {} catch(e) {... Error parsing JSON");
      data = {};
    }
    
    _log2Console("in wss.on('connection', function (connection) { connection.on('message', function (message) { : Parsed Message:", data);
    _log2Console("in wss.on('connection', function (connection) { connection.on('message', function (message) { : Users Object before switch statement:", users);

    switch (data.type) {
      case "login":
        
        _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"login\" : User Tried logging in as data.name: ", data.name);
        
        if (users[data.name]) {
          //someone else is already logged in with the username
          _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"login\" if(users[data.name]) {...} : someone else is already logged in with the username. ");
          sendTo(connection, {
            type: "login",
            success: false
          });
        } else {
           _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"login\" if(users[data.name]) {...} else {... : nobody else has logged in with the username; the username is free. "); 
           //nobody else has logged in with the username; the username is free
          users[data.name] = connection;
          connection.name = data.name;
          sendTo(connection, {
            type: "login",
            success: true
          });
        }
        _log2Console("in wss.on('connection', .... { connection.on('message' ....: Users Object end of switch-case-login:", users);
        
        break;
      case "offer":
        _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"offer\" : Sending offer to data.name: ", data.name);
        var conn = users[data.name];

        if (conn != null) {
          _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"offer\" if(conn != null) {...} : connection is available for other user; send an offer to the other user. ", data.offer);
          connection.otherName = data.name;
          sendTo(conn, {
            type: "offer",
            offer: data.offer,
            name: connection.name
          });
        }
        _log2Console("in wss.on('connection', .... { connection.on('message' ....: Users Object end of switch-case-offer:", users);
        break;
      case "answer":
        _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"answer\" : Sending answer to data.name: ", data.name);
        var conn = users[data.name];

        if (conn != null) {
          _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"answer\" if(conn != null) {...} : connection is available for other user; send an answer to the other user. ", data.answer);
          connection.otherName = data.name;
          sendTo(conn, {
            type: "answer",
            answer: data.answer
          });
        }
        _log2Console("in wss.on('connection', .... { connection.on('message' ....: Users Object end of switch-case-answer:", users);
        break;
      case "candidate":
        _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"candidate\" : Sending candidate to data.name: ", data.name);
        var conn = users[data.name];

        if (conn != null) {
          _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"candidate\" if(conn != null) {...} : connection is available for other user; send a candidate to the other user. ", data.candidate);
          sendTo(conn, {
            type: "candidate",
            candidate: data.candidate
          });
        }
        _log2Console("in wss.on('connection', .... { connection.on('message' ....: Users Object end of switch-case-candidate:", users);
        break;
      case "leave":
        _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"leave\" : Sending leave message to Disconnect user from data.name: ", data.name);
        var conn = users[data.name];
        conn.otherName = null;

        if (conn != null) {
          _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { case \"leave\" if(conn != null) {...} : connection is available for other user; send a leave message to the other user. ");
          sendTo(conn, {
            type: "leave"
          });
        }
        _log2Console("in wss.on('connection', .... { connection.on('message' ....: Users Object end of switch-case-leave:", users);
        break;
      default:
        _log2Console("in wss.on('connection', .... { connection.on('message' .... { switch (data.type) { default : Error unknown command data.type '%s' sending error back to client : ", data.type);
        sendTo(connection, {
          type: "error",
          message: "Unrecognized command: " + data.type
        });
        _log2Console("in wss.on('connection', .... { connection.on('message' ....: Users Object end of switch-case-default:", users);
        break;
    }
  });

  connection.on('close', function () {
    _log2Console("in wss.on('connection', function (connection) { connection.on('close', function () { : ");
    if (connection.name) {
      _log2Console("in wss.on('connection', function (connection) { connection.on('close', function () { if (connection.name) { delete users[connection.name]; ");
      delete users[connection.name];

      if (connection.otherName) {
        _log2Console("in wss.on('connection', function (connection) { connection.on('close', function () { if (connection.name) { if (connection.otherName) { : Disconnecting user from ", connection.otherName);

        var conn = users[connection.otherName];
        conn.otherName = null;

        if (conn != null) {
          _log2Console("in wss.on('connection', function (connection) { connection.on('close', function () { if (connection.name) { if (connection.otherName) { if (conn != null) { : connection is available for other user; send a leave message to the other user. ");  
          sendTo(conn, {
            type: "leave"
          });
        }
      }
    }
    _log2Console("wss.connection.on('close'): Users Object end of handler:", users);
  });
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

function sendTo(conn, message) {
  conn.send(JSON.stringify(message));
}

wss.on('listening', function () {
    _log2Console("Server started...");
});

/**
 * 
 * Calculates the current date & time & returns it in YYYY-Mmm-dd hh:mm:ss format.
 * 
 * @returns String current date & time in YYYY-Mmm-dd hh:mm:ss format.
 */
function _getCurrentTimeStamp() {

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();
    var seconds = d.getSeconds();
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    seconds = (seconds < 10) ? '0' + seconds : seconds;

    return year + '-' + months[month] + '-' + day + ' ' + d.getHours() + ':' 
           + d.getMinutes() + ':' + seconds;
}

/**
 * 
 * Wrapper for console.log that adds timestamp to each print out.
 * 
 * Accepts 0 or more arguments.
 * 
 * @returns void
 */
function _log2Console() {

    if (arguments.length < 1) {
        return; // nothing to do no arguments supplied
    }

    var timestamp = '[' + _getCurrentTimeStamp() + ']';

    if (arguments.length === 1) {

        ////////////////////////
        //one argument supplied
        ////////////////////////
        console.log(timestamp, ' ', arguments[0]);

    } else if (arguments.length > 1) {

        //////////////////////////////////
        //more than one argument supplied
        //////////////////////////////////

        //convert arguments object to an array
        var args_as_array = Array.prototype.slice.call(arguments);

        //convert 1st arg to string prepended with %s
        args_as_array[0] = '%s ' + String(args_as_array[0]);

        //make timestamp the 2nd argument & shift other elements 1 position down
        var rearranged_args = [args_as_array.shift(), timestamp].concat(args_as_array);

        //call console.log with the modified arguments
        console.log.apply(null, rearranged_args);
    }
}