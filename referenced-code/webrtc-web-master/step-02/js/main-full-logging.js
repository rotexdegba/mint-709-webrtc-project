'use strict';

var startButton = document.getElementById('startButton');
startButton.onclick = start;

var callButton = document.getElementById('callButton');
callButton.onclick = call;
callButton.disabled = true;

var hangupButton = document.getElementById('hangupButton');
hangupButton.onclick = hangup;
hangupButton.disabled = true;

var startTime;
var localVideo = document.getElementById('localVideo');
var remoteVideo = document.getElementById('remoteVideo');

var localStream;
var pc1;
var pc2;
var offerOptions = {
  offerToReceiveAudio: 1,
  offerToReceiveVideo: 1
};

function start() {
  trace('function start(): Requesting local stream');
  startButton.disabled = true;
  navigator.mediaDevices.getUserMedia({
    audio: false,
    video: true
  })
  .then(gotStream)
  .catch(function(e) {
    alert('getUserMedia() error: ' + e.name);
  });
}

function gotStream(stream) {
  trace('function gotStream(stream): Received local stream');
  localVideo.srcObject = stream;
  trace('function gotStream(stream): After localVideo.srcObject = stream;');
  // Add localStream to global scope so it's accessible from the browser console
  window.localStream = localStream = stream;
  callButton.disabled = false;
}

function call() {
  callButton.disabled = true;
  hangupButton.disabled = false;
  trace('function call(): Starting call');
  startTime = window.performance.now();
  var videoTracks = localStream.getVideoTracks();
  var audioTracks = localStream.getAudioTracks();
  if (videoTracks.length > 0) {
    trace('function call(): Using video device: ' + videoTracks[0].label);
  }
  if (audioTracks.length > 0) {
    trace('function call(): Using audio device: ' + audioTracks[0].label);
  }
  var servers = null;
  // Add pc1 to global scope so it's accessible from the browser console
  window.pc1 = pc1 = new RTCPeerConnection(servers);
  trace('function call(): Created local peer connection object pc1');
  pc1.onicecandidate = function(e) {
    onIceCandidate(pc1, e);
  };
  // Add pc2 to global scope so it's accessible from the browser console
  window.pc2 = pc2 = new RTCPeerConnection(servers);
  trace('function call(): Created remote peer connection object pc2');
  pc2.onicecandidate = function(e) {
    onIceCandidate(pc2, e);
  };
  pc1.oniceconnectionstatechange = function(e) {
    onIceStateChange(pc1, e);
  };
  pc2.oniceconnectionstatechange = function(e) {
    onIceStateChange(pc2, e);
  };
  pc2.onaddstream = function (e) {
    // Add remoteStream to global scope so it's accessible from the browser console
    window.remoteStream = remoteVideo.srcObject = e.stream;
    trace('function call(): pc2.onaddstream = function (e): pc2 received remote stream');
  };

  pc1.addStream(localStream);
  trace('function call(): Added local stream to pc1');

  trace('function call(): pc1 createOffer start');
  pc1.createOffer(
    offerOptions
  ).then(
    onCreateOfferSuccess,
    function (error) {
        trace('function call(): pc1.createOffer(..).then(s, e): Failed to create session description: ' + error.toString());
    }
  );
}

function hangup() {
  trace('function hangup(): Ending call');
  pc1.close();
  pc2.close();
  pc1 = null;
  pc2 = null;
  hangupButton.disabled = true;
  callButton.disabled = false;
}

localVideo.addEventListener('loadedmetadata', function() {
  trace("localVideo.addEventListener('loadedmetadata', function(): Local video videoWidth: " + this.videoWidth +
    'px,  videoHeight: ' + this.videoHeight + 'px');
});

remoteVideo.addEventListener('loadedmetadata', function() {
  trace("remoteVideo.addEventListener('loadedmetadata', function(): Remote video videoWidth: " + this.videoWidth +
    'px,  videoHeight: ' + this.videoHeight + 'px');
});

remoteVideo.onresize = function() {
  trace("remoteVideo.onresize = function(): Remote video size changed to " +
    remoteVideo.videoWidth + 'x' + remoteVideo.videoHeight);
  // We'll use the first onresize callback as an indication that video has started
  // playing out.
  if (startTime) {
    var elapsedTime = window.performance.now() - startTime;
    trace("remoteVideo.onresize = function(): Setup time: " + elapsedTime.toFixed(3) + 'ms');
    startTime = null;
  }
};

function getName(pc) {
  return (pc === pc1) ? 'pc1' : 'pc2';
}

function getOtherPc(pc) {
  return (pc === pc1) ? pc2 : pc1;
}

function onCreateOfferSuccess(desc) {
  trace('function onCreateOfferSuccess(desc): Offer from pc1\n' + desc.sdp);
  trace('function onCreateOfferSuccess(desc): pc1 setLocalDescription start');
  pc1.setLocalDescription(desc).then(
    function() {
      trace('function onCreateOfferSuccess(desc): ' + getName(pc1) + ' setLocalDescription complete');
    },
    function (error) {
      trace('function onCreateOfferSuccess(desc): pc1.setLocalDescription(desc).then(s, e): Failed to set session description: ' + error.toString());
    }
  );
  trace('function onCreateOfferSuccess(desc): pc2 setRemoteDescription start');
  pc2.setRemoteDescription(desc).then(
    function() {
      trace('function onCreateOfferSuccess(desc): ' + getName(pc2) + ' setRemoteDescription complete');
    },
    function (error) {
      trace('function onCreateOfferSuccess(desc): pc2.setRemoteDescription(desc).then(s, e): Failed to set session description: ' + error.toString());
    }
  );
  trace('function onCreateOfferSuccess(desc): pc2 createAnswer start');
  // Since the 'remote' side has no media stream we need
  // to pass in the right constraints in order for it to
  // accept the incoming offer of audio and video.
  pc2.createAnswer().then(
    onCreateAnswerSuccess,
    function (error) {
        trace('function onCreateOfferSuccess(desc): pc2.createAnswer().then(s, e): Failed to create session description: ' + error.toString());
    }
  );
}

function onCreateAnswerSuccess(desc) {
  trace('function onCreateAnswerSuccess(desc): Answer from pc2:\n' + desc.sdp);
  trace('function onCreateAnswerSuccess(desc): pc2 setLocalDescription start');
  pc2.setLocalDescription(desc).then(
    function() {
      trace('function onCreateAnswerSuccess(desc): ' + getName(pc2) + ' setLocalDescription complete')
    },
    function (error) {
      trace('function onCreateAnswerSuccess(desc): pc2.setLocalDescription(desc).then(s, e): Failed to set session description: ' + error.toString());
    }
  );
  trace('function onCreateAnswerSuccess(desc): pc1 setRemoteDescription start');
  pc1.setRemoteDescription(desc).then(
    function() {
      trace('function onCreateAnswerSuccess(desc): ' + getName(pc1) + ' setRemoteDescription complete')
    },
    function (error) {
      trace('function onCreateAnswerSuccess(desc): pc1.setRemoteDescription(desc).then(s, e): Failed to set session description: ' + error.toString());
    }
  );
}

function onIceCandidate(pc, event) {
  if (event.candidate) {
    getOtherPc(pc).addIceCandidate(
      new RTCIceCandidate(event.candidate)
    ).then(
      function() {
        trace('function onIceCandidate(pc, event): ' + getName(pc) + ' addIceCandidate success');
      },
      function(err) {
        trace('function onIceCandidate(pc, event): ' + getName(pc) + ' failed to add ICE Candidate: ' + err.toString())
      }
    );
    trace('function onIceCandidate(pc, event): ' + getName(pc) + ' ICE candidate: \n' + event.candidate.candidate);
  }
}

function onIceStateChange(pc, event) {
  if (pc) {
    trace('function onIceStateChange(pc, event): ' + getName(pc) + ' ICE state: ' + pc.iceConnectionState);
    console.log('ICE state change event: ', event);
  }
}

function trace(text) {
  if (text[text.length - 1] === '\n') {
    text = text.substring(0, text.length - 1);
  }
  if (window.performance) {
    var now = (window.performance.now() / 1000).toFixed(3);
    console.log(now + ': ' + text);
  } else {
    console.log(text);
  }
}
